.PHONY: default
#default: devserver
default: test


.PHONY: devserver
devserver: init-db routes
	flask --app aocp run --debug --host=0.0.0.0

.PHONY: routes
routes:
	flask --app aocp routes

.PHONY: test-verifier
test-verifier:
	flask --app aocp --debug test-verifier

.PHONY: init-db
init-db:
	flask --app aocp init-db


.PHONY: reset-db
reset-db:
	$(RM) instance/aocp.sqlite
	flask --app aocp init-db

.PHONY: test
test: test-validation test-aoc-py test-verifier test-score-py

.PHONY: test-validation
test-validation:
	python3 -m aocp.validation

.PHONY: test-aoc-py
test-aoc-py:
	flask --app aocp --debug test-aoc-py

.PHONY: test-score-py
test-score-py:
	flask --app aocp --debug test-score-py

.PHONY: get-winners
get-winners:
	flask --app aocp --debug get-winners
