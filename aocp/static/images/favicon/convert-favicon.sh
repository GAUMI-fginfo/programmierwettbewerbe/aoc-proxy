#!/bin/bash


set -e


function convertfavicon() {
	targetsize="${1}"
	#inkscape -z -e "favicon-${targetsize}.png" -w "${targetsize}" -h "${targetsize}" favicon.svg
	inkscape --export-area-snap --export-type png --export-filename "favicon-${targetsize}.png" -w "${targetsize}" -h "${targetsize}" favicon.svg
}

convertfavicon 32
convertfavicon 57
convertfavicon 72
convertfavicon 114
convertfavicon 144

convert favicon-144.png -channel RGB -negate favicon-144-negate.png
