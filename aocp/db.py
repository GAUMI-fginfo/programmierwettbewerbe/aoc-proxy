from . import auth

import sqlite3
import click
import pandas as pd
from typing import Tuple
from flask import current_app, g
from zoneinfo import ZoneInfo


class DB:
    """
    Access and interact with the database.

    Get an instance of this class via get_db().

    """

    def __init__(self, cnx: sqlite3.Connection):
        """
        Don't initialize DB yourself. Get an instance of this class via get_db().
        """
        self.cnx: sqlite3.Connection = cnx
        # self.cnx.row_factory = sqlite3.Row
        self.is_closed = False

        # Set PRAGMAs
        cursor = self.cnx.cursor()
        cursor.execute("PRAGMA foreign_keys=ON")  # Enforce foreign_keys
        cursor.close()

    def init(self):
        """
        Initialize the database.

        This creates all the tables.
        """
        with current_app.open_resource('schema.sql') as f:
            self.cnx.executescript(f.read().decode('utf8'))

    def close(self):
        """
        Close the database connection.

        Flask will call this function automatically on teardown.
        """
        if not self.is_closed:
            self.cnx.close()
            self.is_closed = True

    def login_or_register_user(self, gitlab_id: str, gitlab_email: str) -> Tuple[str, bool]:
        """
        Login or register a user via the database.

        Parameters:
        gitlab_id    -- The users id on the identity provider.
        gitlab_email -- The users email address on the identity provider.

        Returns: (user_id, first_login)
        user_id     -- The users id in our database.
        first_login -- Whether the account has just been registered (True), or was already known (False).

        Throws:
        - AuthException if login or register fails (should never happen, unless something is wrong with the database)
        """

        first_login = True

        # Try to register user
        try:
            with self.cnx:
                self.cnx.execute("INSERT OR FAIL INTO users (gitlabid,email) VALUES (?,?)", (gitlab_id, gitlab_email))
        except sqlite3.IntegrityError:
            # User has already been registered
            first_login = False

        # Try to login
        with self.cnx:
            cursor = self.cnx.execute("SELECT userid FROM users WHERE gitlabid = ?", (gitlab_id,))
            res: sqlite3.Row = cursor.fetchone()

            if not res:
                raise auth.AuthException("Failed to login or register user.", gitlab_id, gitlab_email, first_login)

            # Successful login
            userid: str = res[0]
            return (userid, first_login)

    def fetch_all_users(self) -> pd.DataFrame:
        """Fetch information about all users.

        Returns: A DataFrame with the following columns:
        userid   -- str        -- The users id in our database. (Index)
        email    -- str        -- The users email address.
        username -- str | None -- The users display name.
        groupid  -- str | None -- The id of the group the user is currently a memeber of or None.
        admin    -- bool       -- Whether the user has admin permissions.
        gitlabid -- str        -- The users gitlabid.
        """
        query = "SELECT ALL userid,email,username,groupid,admin,gitlabid FROM users"
        df = pd.read_sql_query(query, self.cnx, index_col="userid")
        df = df.astype({"admin": bool})
        # print("==="*5)
        # print(df.dtypes)
        # print(df)
        # print("===")
        # print(df.iloc[0])
        # print(df.iloc[0]["email"])
        # print(type(df.iloc[0]["email"]))
        # print("===")
        return df

    def delete_user(self, userid: str):
        """Delete a user.

        All submissions and groups made by that user will also be deleted.

        Parameters:
        userid -- The id of the user to delete.
        """
        with self.cnx:
            query = "DELETE FROM users WHERE userid = ? LIMIT 1"
            self.cnx.execute(query, (userid,))

    def update_user_name(self, userid: str, username: str | None):
        """Change or delete a username.

        Parameters:
        userid   -- The id of the user to edit.
        username -- The new username or None if you want to remove it.
        """
        with self.cnx:
            query = "UPDATE users SET username = ? WHERE userid = ?"
            self.cnx.execute(query, (username, userid))

    def update_user_group(self, userid: str, groupid: str | None):
        """Change which group the user is a memeber of or just remove them from the current group.

        Parameters:
        userid  -- The id of the user to edit.
        groupid -- The new groupid or None if the user should be in no group.
        """
        with self.cnx:
            query = "UPDATE users SET groupid = ? WHERE userid = ?"
            self.cnx.execute(query, (groupid, userid))

    def update_user_admin(self, userid: str, admin: bool):
        """Update whether the user is an admin or not.

        Parameters:
        userid   -- The id of the user to edit.
        admin    -- Whether the user is supposed to be an admin.
        """
        with self.cnx:
            adminint = 0
            if admin is True:  # Very explicit as a mistake here could be fatal.
                adminint = 1
            query = "UPDATE users SET admin = ? WHERE userid = ?"
            self.cnx.execute(query, (adminint, userid))

    def set_day_puzzle(self, day: int, puzzle: bytes):
        """Set the puzzle for a day.

        This function does nothing if a puzzle is already set for that day.

        Parameters:
        day      -- The AOC day. (1-25)
        puzzle   -- The puzzle for that day.

        See also:
        - fetch_day_puzzle()
        """
        with self.cnx:
            query = "INSERT INTO days (day,puzzle) VALUES (?,?) ON CONFLICT DO NOTHING"
            self.cnx.execute(query, (day, puzzle))

    def fetch_day_puzzle(self, day: int) -> bytes | None:
        """Fetch the puzzle for a day.

        Returns:
        Either the puzzle as bytes or None if no puzzle is known for that day.

        See also:
        - set_day_puzzle()
        """
        with self.cnx:
            query = "SELECT puzzle FROM days WHERE day = ?"
            cursor = self.cnx.execute(query, (day,))
            res: sqlite3.Row = cursor.fetchone()
            if res:
                return res[0]  # Puzzle found
            else:
                return None  # No puzzle known

    def set_day_solution(self, day: int, solution: str):
        """Set the solution for a day.

        Bug: This function does nothing IF a puzzle hasn't been set for that day yet using set_day_puzzle().

        Parameters:
        day      -- The AOC day. (1-25)
        solution -- The correct solution for that day.

        See also:
        - fetch_day_solution()
        - set_day_puzzle()
        """
        with self.cnx:
            query = "UPDATE days SET solution = ? WHERE day = ?"
            self.cnx.execute(query, (solution, day))

    def fetch_day_solution(self, day: int) -> str | None:
        """Fetch the solution for a day.

        Returns:
        Either the solution as a string or None if no solution is known for that day.

        See also:
        - set_day_solution()
        """
        with self.cnx:
            query = "SELECT solution FROM days WHERE day = ?"
            cursor = self.cnx.execute(query, (day,))
            res: sqlite3.Row = cursor.fetchone()
            if res:
                return res[0]  # Solution found
            else:
                return None  # No solution known

    def add_submission(self, userid: str, day: int, value: str):
        """Add a users submission for a specific day.

        The timestamp of the submission will automatically be generated in the database.
        The status of the submission will be set to "PENDING".
        The group of the submission will automatically be set to the current group of the user.

        This function does nothing if the user already submitted this value for this day

        Parameters:
        userid -- The users id in our database.
        day    -- The AOC day. (1-25)
        value  -- The submission value.
        """
        with self.cnx:
            # fetch the groupid the user is currently in
            query = "SELECT groupid FROM users WHERE userid = ?"
            cursor = self.cnx.execute(query, (userid,))
            res: sqlite3.Row = cursor.fetchone()
            groupid = None
            if res:
                groupid = res[0]

            query = "INSERT INTO submissions (userid,day,value,groupid) VALUES (?,?,?,?) ON CONFLICT DO NOTHING"
            self.cnx.execute(query, (userid, day, value, groupid))

    def update_submission_status(self, userid: str, day: int, value: str, status: str):
        """Update the status of a submission.

        This function CANNOT be used to add a submission. Use add_submission() instead.

        Parameters:
        userid -- The users id in our database.
        day    -- The AOC day. (1-25)
        value  -- The submission value.
        status -- The new status. Must be one of:
                  - PENDING
                  - INVALID
                  - CORRECT
                  - INCORRECT
        """
        with self.cnx:
            query = "UPDATE submissions SET status = ? WHERE userid = ? AND day = ? AND value = ?"
            self.cnx.execute(query, (status, userid, day, value))

    def fetch_submissions(self, userid: str | None = None, status: str | None = None, day: int | None = None, groupid: str | None = None) -> pd.DataFrame:
        """Fetch multiple submissions.

        The submissions can be filtered using one or more of the parameters.
        All submissions will be fetched, if none of the parameters are set.

        Parameters:
        userid   -- If set, only fetch submissions by this user.
        status   -- If set, only fetch submissions with this status.
        day      -- If set, only fetch submissions for this day.
        groupid  -- If set, only fetch submissions for this group.

        Returns: A multiindexed DataFrame with the following columns:
        day       -- int              -- The AOC day. (Index)
        userid    -- str              -- The users id in our database. (Index)
        value     -- str              -- The submission value. (Index)
        status    -- str              -- The status of the submission.
        timestamp -- pandas.Timestamp -- Date and time of the submission. (Timezone: Europe/Berlin) (Ordered by ascending)
        groupid   -- str | None       -- The id of the group the submission was made for.
        """
        query = "SELECT ALL day,userid,value,status,timestamp,groupid FROM submissions"

        # Add filters to query
        filters = []
        if userid is not None:
            filters.append("userid = :userid")
        if status is not None:
            filters.append("status = :status")
        if groupid is not None:
            filters.append("groupid = :groupid")
        if day is not None:
            filters.append("day = :day")

        if len(filters) > 0:
            query += " WHERE "
            query += " AND ".join(filters)

        query += " ORDER BY timestamp ASC"

        # Run SQL query
        params = {
            "userid": userid,
            "status": status,
            "day": day,
            "groupid": groupid
        }
        df = pd.read_sql_query(query, self.cnx, index_col=["userid", "day", "value"], params=params)

        # Convert timestamp to datetime with correct timezone
        tz = ZoneInfo("Europe/Berlin")
        timestamps = pd.to_datetime(df["timestamp"], utc=True, unit="s", origin="unix")
        # timestamps = timestamps.tz_convert("Europe/Berlin") # Bugged.
        timestamps = timestamps.map(lambda d: d.astimezone(tz=tz))
        # Store the timestamps as a series of objects instead of explicitly saying they are datetime objects, because otherwise pandas will convert them to numpy.datetime64 objects.
        df["timestamp"] = pd.Series(timestamps, dtype=object)

        return df

    def create_group(self, userid: str, groupid: str, color: str):
        """Create a new group.

        Parameters:
        userid  -- The id of the user creating this group.
        groupid -- The id the group should have. This is also the groups name.
        color   -- The color the group should have.

        Throws:
        - sqlite3.IntegrityError: If a group by that id already exists.
        """
        with self.cnx:
            self.cnx.execute("INSERT OR FAIL INTO groups (groupid,creator,color) VALUES (?,?,?)", (groupid, userid, color))

    def update_group(self, groupid: str, color: str):
        """Update the color of a group.

        Parameters:
        groupid -- The id of the group to edit.
        color   -- The color the group should have.
        """
        with self.cnx:
            query = "UPDATE OR FAIL groups SET color = ? WHERE groupid = ?"
            self.cnx.execute(query, (color, groupid))

    def delete_group(self, groupid: str):
        """Delete a group.

        All submissions made for that group will forget their group association.

        Parameters:
        groupid -- The id of the group to delete.
        """
        with self.cnx:
            query = "DELETE FROM groups WHERE groupid = ? LIMIT 1"
            self.cnx.execute(query, (groupid,))

    def fetch_all_groups(self) -> pd.DataFrame:
        """Fetch information about all groups.

        Returns: A DataFrame with the following columns:
        groupid -- str -- The group id in our database. (Index)
        creator -- str -- The id of the user that created this group.
        color   -- str -- The color the group has.
        """
        query = "SELECT ALL groupid,creator,color FROM groups"
        df = pd.read_sql_query(query, self.cnx, index_col="groupid")
        return df


def get_db() -> DB:
    """
    Get a connection to the database.

    Connections get cached, so you can call this function as often as you want.
    """
    if 'db' not in g:
        g.db = DB(sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        ))
    return g.db


def close_db(e=None):
    """
    Close the database connection.

    Flask will call this function automatically on teardown.
    """
    db = g.pop('db', None)

    if db is not None:
        db.close()


@click.command('init-db')
def init_db_command():
    """Clear the existing data and create new tables."""
    get_db().init()
    click.echo('Initialized the database.')


def init_app(app):
    """
    Gets called by __init__.py on initialization of the flask App.
    """
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
