from datetime import datetime
from . import db as database
import click
from . import aoc
from flask import current_app
from . import verifier
from typing import Tuple, cast


def calculate_points(day: int, timestamp: datetime, is_first: bool) -> int:
    """
    Calculates the points based on day, time and if the solver was first.

    Parameters:
    day       -- Day that the solution was submitted for.
    timestamp -- Actual timestamp of the solution.
    is_first  -- Boolean (True if person was first to solve problem on the day).

    Returns:
    score -- the score or zero if the submission was made on the wrong day or outside of the event
    """
    sub_day, sub_hour = aoc.get_aoc_day(timestamp)

    if sub_day != day or sub_day <= 0 or sub_day >= 26:
        return 0  # Submission was made on the wrong day or outside of the event

    score = 0

    # base points by day: (all ranges inclusive)
    # 1-5: 1 point
    # 6-10: 2 points
    # 11-15: 3 points
    # 16-20: 4 points
    # 21-25: 5 points
    score += ((day - 1) // 5) + 1

    # bonus time points by hour since start of day: (all ranges inclusive)
    # 0-7: 2 points
    # 8-15: 1 point
    # 16-23: 0 points
    score += 2 - (sub_hour // 8)

    # bonus point if person is first to solve on given day.
    if is_first:
        score += 1
    # if score > 0:
        # current_app.logger.debug("HOUR:%d, SCORE:%d,TIMESTAMP:%s,DAY:%d",sub_hour,score,timestamp,sub_day)
    return score


def get_first(day: int) -> str | None:
    """
    takes an AOC day (1-25) as an integer and checks who was the first to solve the puzzle on that day.
    returns: the userid of the first solver or None if there are no solvers for the given day.
    """
    corrects = database.get_db().fetch_submissions(status="CORRECT", day=day)
    if corrects.empty:
        return None
    corrects = corrects.sort_values(by="timestamp")
    userid = str(corrects.head(1).index[0][0])
    return userid


def get_leaderboard(groupid: str | None = None) -> Tuple[dict, list[dict]]:
    """
    Get the compiled data for building the leaderboard.

    Is a user has made no relevant correct submissions, they will not appear in the result.

    Parameters:
    groupid  -- If set, only fetch the data for building the leaderboard for this group.

    Returns a tuple:
    - A dict of the solves and score for each person:
    {
        userid: {
            userid:str
            total:int
            meansubmissionseconds:int
            days:{
                day:{first:bool,score:int,solved:bool,groupid:str|None}
            }
        }
    }
    - The leaderboard as a list of the person dicts sorted (descending) by 'total' (tiebreaks are done using 'meansubmissionseconds'):
    [
        {
            userid:str
            total:int
            meansubmissionseconds:int
            days:{
                day:{first:bool,score:int,solved:bool,groupid:str|None}
            }
        },
        ...
    ]
    """
    solves = {}
    df = database.get_db().fetch_submissions(status="CORRECT", groupid=groupid)
    # df = df.sort_values(by=["userid","day"])
    first_solvers = {day: get_first(day) for day in range(1, 26)}

    for index, sub in df.iterrows():
        (uid, day, _) = cast(tuple, index)
        first = bool(first_solvers[day] == uid)
        points = calculate_points(day=day, timestamp=sub.timestamp, is_first=first)
        # print(uid," ",day,"   ",points)
        if uid not in solves:
            solves[uid] = {"userid": uid, "total": 0, "days": {day: {"groupid": None, "first": False, "score": 0, "solved": False} for day in range(1, 26)}}
        solves[uid]["days"][day]["score"] = points
        solves[uid]["days"][day]["first"] = first
        solves[uid]["days"][day]["solved"] = True
        solves[uid]["days"][day]["groupid"] = sub.groupid
        solves[uid]["total"] += points

    # Determine mean submission seconds for each person
    meanseconds = df["timestamp"].map(aoc.get_aoc_submission_seconds).groupby(level="userid").mean()
    for uid, meansecond in meanseconds.items():
        solves[uid]["meansubmissionseconds"] = int(meansecond)

    # Build leaderboard
    leaderboard = list(solves.values())
    # Tiebreak via mean submission seconds
    leaderboard = sorted(leaderboard, key=lambda d: d['meansubmissionseconds'])
    # Sort by total points
    leaderboard = sorted(leaderboard, key=lambda d: d['total'], reverse=True)

    return solves, leaderboard


def test_user_submissions():
    userid_peter, _ = database.get_db().login_or_register_user("123", "peter@example.com")
    database.get_db().add_submission(userid_peter, 3, "Hallo")
    database.get_db().add_submission(userid_peter, 3, "Welt")
    database.get_db().add_submission(userid_peter, 3, "Yeet")
    database.get_db().add_submission(userid_peter, 3, "123")


@click.command('test-score-py')
def test_score_py_command():
    click.echo('Adding Submissions')
    # submit_answer(2022,1,1,"30")
    database.get_db().set_day_puzzle(day=3, puzzle=b'203nsk20')
    database.get_db().set_day_puzzle(day=7, puzzle=b'2138')
    database.get_db().set_day_puzzle(day=20, puzzle=b'abcabca')
    database.get_db().set_day_puzzle(day=1, puzzle=b'aksjndaks')
    database.get_db().set_day_puzzle(day=21, puzzle=b'test')
    database.get_db().set_day_puzzle(day=24, puzzle=b'test2')
    database.get_db().set_day_solution(3, solution="123")
    database.get_db().set_day_solution(7, solution="13")
    database.get_db().set_day_solution(20, solution="abcabca")
    database.get_db().set_day_solution(21, solution="12")
    database.get_db().set_day_solution(24, solution="10")
    test_user_submissions()
    click.echo('Verifying...')
    current_app.logger.debug(verifier.verify())
    click.echo('testing get-leaderboard')
    solves, leaderboard = get_leaderboard()
    current_app.logger.debug("Solves:")
    current_app.logger.debug(solves)
    current_app.logger.debug("Leaderboard:")
    current_app.logger.debug(leaderboard)


def init_app(app):
    app.cli.add_command(test_score_py_command)
