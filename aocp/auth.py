from urllib.parse import urlencode, quote_plus
from . import db

from flask import Flask, Blueprint, current_app, session, url_for, redirect

from authlib.integrations.flask_client import OAuth, FlaskOAuth2App, OAuthError

from typing import cast

# Based on: https://developer.auth0.com/resources/guides/web-app/flask/basic-authentication


class AuthException(Exception):
    pass


bp = Blueprint('auth', __name__, url_prefix='/auth')


def init_app(app: Flask):
    global oauth_gitlab
    auth_config = app.config['AUTH']
    oauth = OAuth(app)
    # oauth_gitlab : FlaskOAuth2App = cast(FlaskOAuth2App, oauth.register(
    oauth_gitlab = cast(FlaskOAuth2App, oauth.register(
        "gitlab",
        client_id=auth_config["CLIENT_ID"],
        client_secret=auth_config["CLIENT_SECRET"],
        client_kwargs={
            "scope": auth_config["SCOPE"]
        },
        server_metadata_url=auth_config["SERVER_METADATA_URL"]
    ))


@bp.route("/callback", methods=["GET", "POST"])
def callback():
    """
    Callback redirect from the identity provider.
    """

    try:
        token = oauth_gitlab.authorize_access_token()  # Extract and fetch all information about the user
    except OAuthError as e:
        # Identity provider denied authentication (probably by user choice)
        current_app.logger.warning(e)
        # TODO return a rendered template with an actual correct status code instead of just a redirect
        return redirect("/?" + urlencode(
            {
                "error": str(e.error),
                "error_description": str(e.description)
            },
            quote_via=quote_plus,
        ), code=302)

    """
    Here is an example token:
        {
            'access_token': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
            'created_at': 0000000000,
            'expires_at': 0000000000,
            'expires_in': 7200,
            'id_token': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
            'scope': 'openid email',
            'token_type': 'Bearer',
            'userinfo': {
                'aud': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
                'auth_time': 0000000000,
                'email': 'xxxxxxxxxxxxx@stud.uni-goettingen.de',
                'email_verified': True,
                'exp': 0000000000,
                'groups_direct': ['...'],
                'iat': 0000000000,
                'iss': 'https://gitlab.gwdg.de',
                'name': 'Jake',
                'nickname': 'xxxxxxxxxxxxx',
                'nonce': 'xxxxxxxxxxxxxxxxxxxx',
                'picture': 'https://gitlab.gwdg.de/uploads/-/system/user/avatar/3151/avatar.png',
                'preferred_username': 'xxxxxxxxxxxxx',
                'profile': 'https://gitlab.gwdg.de/xxxxxxxxxxxxx',
                'sub': '3151',
                'sub_legacy': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
            }
        }
    """

    # Extract the information we need.
    gitlab_id = str(token["userinfo"]["sub"])
    gitlab_email = str(token["userinfo"]["email"])

    userid, first_login = db.get_db().login_or_register_user(gitlab_id, gitlab_email)

    session["userid"] = userid

    return redirect(f"/?firstlogin={first_login}", code=302)


@bp.route("/login")
def login():
    """
    Redirect to the identity provider login page.
    """
    return oauth_gitlab.authorize_redirect(
        redirect_uri=url_for("auth.callback", _external=True)
    )


@bp.route("/logout")
def logout():
    """
    Clear the session and thus logout the user.

    Note this doesn't log the user out of gitlab.
    """
    session.clear()

    return redirect("/?logout=True", code=302)
