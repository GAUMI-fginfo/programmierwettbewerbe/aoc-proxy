from sqlite3 import IntegrityError
from typing import cast
from . import db
from . import auth
from . import verifier
from . import aoc
from . import validation
from . import context as ctx
from . import score


import os

import json
import pprint

from flask import Blueprint, Flask, abort, current_app, redirect, render_template, request, session, url_for, send_file
from flask_apscheduler import APScheduler
from werkzeug.wrappers import Response
from werkzeug.wrappers.response import Response as WSGIResponse  # noqa: F811
from werkzeug.exceptions import BadRequest, Conflict, Forbidden, HTTPException

import matplotlib
from matplotlib.figure import Figure
matplotlib.use("Agg")


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'aocp.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    config_path = os.path.join(app.instance_path, 'config.json')
    if os.path.exists(config_path):
        print("Loading config from", config_path)
        app.config.from_file(config_path, load=json.load)
    else:
        print("WARN: Config file not found:", config_path)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    if app.config["DEBUG"]:
        print("APP Instance Path:", app.instance_path)
        print("Config:")
        pprint.pprint(dict(app.config))

    @app.errorhandler(HTTPException)
    def handle_exception(e: HTTPException):
        # start with the correct headers and status code from the error
        code = e.code
        if e.code is None:
            code = 500
        try:
            context = ctx.build_context(app, ctx.ContextReq.DEFAULT)
        except Exception as ce:
            current_app.logger.error("Couldn't build context for error page: %s - %s", str(e), str(ce))
            context = {}
        context["error"] = {
            "code": code,
            "name": e.name,
            "description": e.description,
        }
        resp = cast(WSGIResponse, e.get_response())
        resp.content_type = "text/html; charset=utf-8"
        try:
            resp.set_data(render_template("error.html", **context))
        except Exception:
            current_app.logger.error("Failed to render the error template!")
        return resp

    @app.route('/day/<int:day>/input', methods=["GET"])
    def puzzle(day: int) -> Response:
        """Get the puzzle/input of the day.
        """
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        day = validation.validate_day(day)
        puzzle = dbpuzzle = db.get_db().fetch_day_puzzle(day)

        if puzzle is None and day <= aoc.get_aoc_day()[0]:
            puzzle = aoc.get_puzzle(app.config["YEAR"], day)

        if puzzle is None:
            abort(404, description="Dieses Puzzle existiert noch nicht. Versuch es später nochmal.")  # Puzzle existiert noch nicht.

        if dbpuzzle is None and puzzle is not None:
            db.get_db().set_day_puzzle(day, puzzle)  # Speicher Puzzle in DB

        daystr = str(day) if day > 9 else "0" + str(day)
        headers = {
            "Content-Disposition": f"inline; filename=\"input_{daystr}.txt\""
        }
        return Response(response=puzzle, status=200, content_type="text/plain; charset=utf-8", direct_passthrough=True, headers=headers)

    @app.route('/day/<int:day>/submit', methods=["POST"])
    def submit(day: int) -> Response:
        """Let a user submit their solution for a day.
        """
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        day = validation.validate_day(day)
        value = request.form.get("value", type=validation.validate_submission_value)
        if value is None:
            raise BadRequest(description="value fehlt")
        if day > aoc.get_aoc_day()[0]:
            raise Forbidden(description="Deine Abgabe ist zu früh.")
        if "userid" not in session:
            # raise Unauthorized(description="Du bist nicht angemeldet.") # Nicht standardkonform.
            raise Forbidden(description="Du bist nicht angemeldet.")
        userid = session["userid"]
        puzzle(day)  # Ensure the referenced puzzle exists in the Database
        db.get_db().add_submission(userid=userid, value=value, day=day)
        return redirect(url_for("submissions"), code=302)

    @app.route('/groups/create', methods=["POST"])
    def create_group() -> Response:
        """Let a user create a group.
        """
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        groupid = request.form.get("groupid", type=validation.validate_groupid)
        if groupid is None:
            raise BadRequest(description="groupid fehlt")
        color = request.form.get("groupcolor", type=validation.validate_color)
        if color is None:
            raise BadRequest(description="groupcolor fehlt")
        if "userid" not in session:
            # raise Unauthorized(description="Du bist nicht angemeldet.") # Nicht standardkonform.
            raise Forbidden(description="Du bist nicht angemeldet.")
        userid = session["userid"]
        groups = db.get_db().fetch_all_groups()
        groups = groups[groups["creator"] == userid]
        grouplimit = 2
        if len(groups) >= grouplimit:
            raise Forbidden(description=f"Du hast bereits deine Maximalanzahl({grouplimit}) von Gruppen erstellt.")
        try:
            db.get_db().create_group(userid, groupid, color)
        except IntegrityError:
            raise BadRequest(description="Gruppe existiert bereits.")
        return redirect(url_for("get_settings"), code=302)

    @app.route('/groups/graph.png', methods=["GET"])
    def get_group_graph() -> Response:
        """Get graph plotting current points of all groups.
        """
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        p = os.path.join(app.instance_path, 'group_graph.png')
        if not os.path.exists(p):
            generate_group_graph()
        if not os.path.exists(p):
            abort(404)
        return send_file(p, mimetype="image/png")

    @app.route('/settings', methods=["POST"])
    def settings() -> Response:
        """Let a user change their settings.
        """
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        username = request.form.get("username", type=validation.validate_username)
        if username is None:
            raise BadRequest(description="username fehlt")
        groupid = request.form.get("group", type=validation.validate_groupid_or_empty)
        if groupid == "":
            groupid = None  # Lösche groupid
        if "userid" not in session:
            # raise Unauthorized(description="Du bist nicht angemeldet.") # Nicht standardkonform.
            raise Forbidden(description="Du bist nicht angemeldet.")
        userid = session["userid"]
        users = db.get_db().fetch_all_users()
        users = users.drop(index=userid)
        if any(users["username"] == username):
            raise Conflict("The username is already in use. Didn't change any setting.")
        db.get_db().update_user_name(userid=userid, username=username)
        db.get_db().update_user_group(userid=userid, groupid=groupid)
        # return redirect(url_for("get_settings"), code=302)
        return redirect("/", code=302)

    @app.route('/settings', methods=["GET"])
    def get_settings() -> Response | str:
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        if "userid" not in session:
            # raise Unauthorized(description="Du bist nicht angemeldet.") # Nicht standardkonform.
            raise Forbidden(description="Du bist nicht angemeldet.")
        context = ctx.build_context(app, ctx.ContextReq.USERID | ctx.ContextReq.USERS | ctx.ContextReq.GROUPS)
        return render_template("settings.html", **context)

    @app.route('/submissions', methods=["GET"])
    def submissions() -> Response | str:
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        if "userid" not in session:
            # raise Unauthorized(description="Du bist nicht angemeldet.") # Nicht standardkonform.
            raise Forbidden(description="Du bist nicht angemeldet.")
        context = ctx.build_context(app, ctx.ContextReq.DEFAULT | ctx.ContextReq.SUBMISSIONS | ctx.ContextReq.LEADERBOARD | ctx.ContextReq.DAY | ctx.ContextReq.GROUPS)
        return render_template("submissions.html", **context)

    @app.route('/groups')
    def groups():
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        context = ctx.build_context(app, ctx.ContextReq.DEFAULT | ctx.ContextReq.GROUPS)
        return render_template("groups.html", **context)

    @app.route('/rules')
    def rules():
        context = ctx.build_context(app, ctx.ContextReq.DEFAULT)
        return render_template("rules.html", **context)

    @app.route('/faq')
    def faq():
        context = ctx.build_context(app, ctx.ContextReq.DEFAULT)
        return render_template("faq.html", **context)

    @app.route('/nutzungsbedingungen')
    def nutzungsbedingungen():
        context = ctx.build_context(app, ctx.ContextReq.DEFAULT)
        return render_template("nutzungsbedingungen.html", **context)

    @app.route('/datenschutz')
    def datenschutz():
        context = ctx.build_context(app, ctx.ContextReq.DEFAULT)
        return render_template("datenschutz.html", **context)

    @app.route('/')
    def index():
        context = ctx.build_context(app, ctx.ContextReq.all())
        # context = ctx.build_context(app, ctx.ContextReq.all() | ctx.ContextReq.TESTING)
        return render_template("index.html", **context)

    # Register fragments
    app.register_blueprint(get_fragments_blueprint(app))

    # Initialize the database
    db.init_app(app)

    # Initialize authentication
    auth.init_app(app)
    if app.config["USERINTERACTIONALLOWED"]:
        app.register_blueprint(auth.bp)

    # Initialize aoc API
    aoc.init_app(app)

    # Initialize verifier
    verifier.init_app(app)

    # Initialize score
    score.init_app(app)

    # initialize scheduler
    if app.config["USERINTERACTIONALLOWED"]:
        scheduler = APScheduler()
        scheduler.init_app(app)

        @scheduler.task('interval', id='do_verifier', seconds=10, misfire_grace_time=900)
        def verifier_job():
            with app.app_context():
                app.logger.debug("Running verifier job")
                verifier.verify()
                app.logger.debug("Finished running verifier job")

        @scheduler.task('interval', id='do_generate_group_graph', seconds=60, misfire_grace_time=900)
        def generate_group_graph_job():
            # Regenerate the group graph every 60 seconds
            with app.app_context():
                generate_group_graph()

        scheduler.start()

    return app


def get_fragments_blueprint(app: Flask):
    bp = Blueprint('fragments', __name__, url_prefix='/fragments')

    @bp.route('/account', methods=["GET"])
    def account() -> Response | str:
        context = ctx.build_context(app, ctx.ContextReq.USERID | ctx.ContextReq.USERS | ctx.ContextReq.GROUPS)
        return render_template("fragments/account.html", **context)

    @bp.route('/leaderboard/', methods=["GET"])
    @bp.route('/leaderboard/<string:groupid>', methods=["GET"])
    def leaderboard(groupid: str | None = None) -> Response | str:
        if groupid is not None:
            groupid = validation.validate_groupid(groupid)
        context = ctx.build_context(app, ctx.ContextReq.USERID | ctx.ContextReq.USERS | ctx.ContextReq.LEADERBOARD | ctx.ContextReq.GROUPS, leaderboard_groupid=groupid)
        return render_template("fragments/leaderboard.html", **context)

    @bp.route('/settings', methods=["GET"])
    def settings() -> Response | str:
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        if "userid" not in session:
            # raise Unauthorized(description="Du bist nicht angemeldet.") # Nicht standardkonform.
            raise Forbidden(description="Du bist nicht angemeldet.")
        context = ctx.build_context(app, ctx.ContextReq.USERID | ctx.ContextReq.USERS | ctx.ContextReq.GROUPS)
        return render_template("fragments/settings.html", **context)

    @bp.route('/submissions', methods=["GET"])
    def submissions() -> Response | str:
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        if "userid" not in session:
            # raise Unauthorized(description="Du bist nicht angemeldet.") # Nicht standardkonform.
            raise Forbidden(description="Du bist nicht angemeldet.")
        context = ctx.build_context(app, ctx.ContextReq.SUBMISSIONS | ctx.ContextReq.USERID | ctx.ContextReq.GROUPS)
        return render_template("fragments/submissions.html", **context)

    @bp.route('/day/', methods=["GET"])
    @bp.route('/day/<int:d>', methods=["GET"])
    def day(d: int | None = None) -> Response | str:
        if not app.config["USERINTERACTIONALLOWED"]:
            raise Forbidden(description="Diese Route ist nur während des Adventskalenders aktiviert.")
        if d is None:
            d = aoc.get_aoc_day()[0]
        elif d != 0 and d != 26:
            d = validation.validate_day(d)
        daylevel = request.args.get("level", default=2, type=int)
        daylevel = min(daylevel, 6)
        daylevel = max(daylevel, 1)
        context = ctx.build_context(app, ctx.ContextReq.LEADERBOARD | ctx.ContextReq.USERID)
        context["day"] = d
        context["daylevel"] = daylevel
        return render_template("fragments/day.html", **context)

    return bp


def generate_group_graph():
    current_app.logger.debug("Generating new group graph")
    group_df = db.get_db().fetch_all_groups()
    solves, leaderboard = score.get_leaderboard()
    group_dict = group_df.to_dict(orient="index")
    group_scores = []
    for groupid, value in group_dict.items():
        s = 0
        for solves in leaderboard:
            s += sum(solves["days"][j]["score"] for j in range(1, 26) if solves["days"][j]["groupid"] == groupid)
        group_scores.append((groupid, s, group_dict[groupid]["color"]))
    group_scores = sorted(group_scores, key=lambda x: x[1], reverse=True)
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    axis.grid(visible=True)
    axis.set_axisbelow(True)
    fingroupids, finscores, fincolors = list(zip(*group_scores))
    axis.bar(fingroupids, finscores, color=fincolors, width=0.5)
    axis.set_xticks(fingroupids)
    axis.set_xticklabels(fingroupids, rotation=30, ha='right')
    axis.set_title("Gesamtpunkte aller Gruppen")
    p = os.path.join(current_app.instance_path, 'group_graph.png')
    fig.savefig(fname=p, bbox_inches="tight")
    current_app.logger.debug("Group graph generated")
