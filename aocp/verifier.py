from . import db as database
from . import aoc

from flask import current_app
from typing import cast
import click
import time


# p consists of: id, timestamp, answer, status,
def verify():
    """
    fetches all currently pending submissions from the database and checks
    if theyre correct. Checks if a solution already exists in the solution db
    and uses it; makes a request to the aoc website otherwise.
    if the submitted answer is correct the status of the submission in the db
    gets updated accordingly.
    if a ratelimit is returned this program will sleep for the remaining time.
    """
    db = database.get_db()  # dataframe
    pending = fetch_all_pending()  # returns a dataframe
    for index, _ in pending.iterrows():
        (uid, day, val) = cast(tuple, index)
        year = current_app.config["YEAR"]
        solution = db.fetch_day_solution(day)  # is equal to todays solution, if there is one. Is None otherwise.
        if solution:
            if solution == val:
                db.update_submission_status(userid=uid, day=day, value=val, status="CORRECT")
            else:
                db.update_submission_status(userid=uid, day=day, value=val, status="INCORRECT")
        else:
            status, timeout = aoc.submit_answer(year, day, 1, val)
            if status == aoc.Status.PASS:
                db.update_submission_status(userid=uid, day=day, value=val, status="CORRECT")
                db.set_day_solution(day, val)
                current_app.logger.info("Found solution for day %d.", day)
            elif status == aoc.Status.FAIL:
                db.update_submission_status(userid=uid, day=day, value=val, status="INCORRECT")
                current_app.logger.info("Tried a solution for day %d, but AOC says its wrong.", day)
            elif status == aoc.Status.RATE_LIMIT:
                current_app.logger.info("AOC has rate limited us for further %d seconds.", timeout)
                if timeout:
                    time.sleep(timeout + 2)
            elif status == aoc.Status.NOT_LOGGED_IN:
                current_app.logger.error("no/invalid aoc session cookie.")
            elif status == aoc.Status.COMPLETED:
                current_app.logger.warning("AOC says day %d was already completed.", day)
                solution = aoc.get_completed_solution(year, day)
                if solution:
                    current_app.logger.info("We were able to extract the known solution for day %d from AOC: %s", day, solution)
                    db.set_day_solution(day, solution)
                else:
                    current_app.logger.error("We were unable to extract the solution for day %d from AOC.", day)
            elif status == aoc.Status.UNKNOWN:
                current_app.logger.error("unknown status returned.")


def fetch_all_pending():
    return database.get_db().fetch_submissions(status="PENDING").sample(frac=1)
    # gets all database entries with the status "pending" as a dataframe


@click.command('test-verifier')
def test_verifier_command():
    """Execute the verifier once for testing."""
    click.echo('Testing verifier...')
    # submit_answer(2022,1,1,"30")
    current_app.logger.debug(database.get_db().set_day_puzzle(day=3, puzzle=b'203nsk20'))
    current_app.logger.debug(database.get_db().set_day_puzzle(day=7, puzzle=b'2138'))
    current_app.logger.debug(database.get_db().set_day_puzzle(day=20, puzzle=b'abcabca'))
    current_app.logger.debug(database.get_db().set_day_puzzle(day=1, puzzle=b'aksjndaks'))
    current_app.logger.debug(database.get_db().set_day_solution(3, solution="123"))
    current_app.logger.debug(database.get_db().set_day_solution(7, solution="13"))
    current_app.logger.debug(database.get_db().set_day_solution(20, solution="abcabca"))
    current_app.logger.debug(verify())


def init_app(app):
    app.cli.add_command(test_verifier_command)
