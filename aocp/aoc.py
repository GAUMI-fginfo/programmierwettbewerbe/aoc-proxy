from . import context as ctx

from typing import Tuple, cast
import dateparser
from datetime import datetime, timezone
from zoneinfo import ZoneInfo
from enum import Enum
from flask import current_app
import requests
import re
import click

from bs4 import BeautifulSoup


def get_timeout(resp: str) -> int:
    """
    extracts a timeout from the response text.
    returns the timeout in sec or 0 if parsing wasnt successful.
    Example: get_timeout("please wait 20m") would return 1200.
    """
    current_time = datetime.now()
    timeout = dateparser.parse(" ".join(re.findall(r'\d+s|\d+m|\d+h', resp)))
    if timeout:
        return (current_time - timeout).seconds
    else:
        return 0


class Status(Enum):
    """
    possible status after sending a request to aoc website.
    gets returned by submit_answer
    """
    PASS = 0
    FAIL = 1
    RATE_LIMIT = 2
    COMPLETED = 3
    NOT_LOGGED_IN = 4
    UNKNOWN = 5


def submit_answer(year: int, day: int, level: int, answer: str) -> tuple[Status, int | None]:
    """
    submits an answer to the aoc website for a given year and day.
    returns a Status according to the result and, if there is a timeout, the timeout in seconds.
    """
    payload = {'level': level, 'answer': answer}
    r = requests.post(
        f'https://adventofcode.com/{year}/day/{day}/answer',
        data=payload,
        headers={
            "User-Agent": 'https://aoc.fg.informatik.uni-goettingen.de/ by fachgruppe@informatik.uni-goettingen.de',
        },
        cookies={'session': current_app.config["SESSION_COOKIE"]}
    )
    response = r.text
    if "That's the right answer" in response:
        # click.echo("CORRECT ANSWER")

        return Status.PASS, None
    elif "That's not the right answer" in response:
        # click.echo("WROOOOONG")

        return Status.FAIL, None
    elif 'You gave an answer too recently' in response:
        timeout = get_timeout(response)
        # click.echo("RATE LIMIT: "+str(timeout))

        return Status.RATE_LIMIT, timeout

    elif 'Did you already complete it?' in response:
        # click.echo("ALREADY COMPLETED")

        return Status.COMPLETED, None

    elif '[Log In]' in response:
        # click.echo("NOT LOGGED IN")

        return Status.NOT_LOGGED_IN, None

    else:
        # click.echo("HMMM- SUS")
        return Status.UNKNOWN, None


def get_completed_solution(year: int, day: int) -> str | None:
    """
    In case submit_answer() returns Status.COMPLETED, this function can be used to extract the solution.
    """
    resp = requests.get("https://adventofcode.com/" + str(year) + "/day/" + str(day) + "", allow_redirects=True, headers={
        "User-Agent": 'https://aoc.fg.informatik.uni-goettingen.de/ by fachgruppe@informatik.uni-goettingen.de',
    }, cookies={'session': current_app.config["SESSION_COOKIE"]})

    if resp.status_code != 200:
        current_app.logger.warning("Couldn't get completed solution for year %d day %d: %d - %s", year, day, resp.status_code, str(resp.text))
        return None

    soup = BeautifulSoup(resp.text, "html.parser")
    # current_app.logger.debug(soup.prettify())
    for p in soup.find_all('p'):
        if "Your puzzle answer was" in p.text:
            # current_app.logger.debug(p.prettify())
            # current_app.logger.debug("'%s'", p.code.text)
            return p.code.text
    return None


def get_puzzle(year: int, day: int) -> bytes | None:
    """
    Returns the puzzle of a day as bytes.
    If the AOC API doesn't return a puzzle None is returned and warning is logged.
    """
    puzzle = requests.get("https://adventofcode.com/" + str(year) + "/day/" + str(day) + "/input", allow_redirects=True, headers={
        "User-Agent": 'https://aoc.fg.informatik.uni-goettingen.de/ by fachgruppe@informatik.uni-goettingen.de',
    }, cookies={'session': current_app.config["SESSION_COOKIE"]})

    if puzzle.status_code == 200:
        return puzzle.content
    else:
        current_app.logger.warning("Couldn't get the puzzle for year %d day %d: %d - %s", year, day, puzzle.status_code, str(puzzle.text))
        return None


@click.command('get-winners')
def get_winners_command():
    """Print information about the top 10 people in the leaderboard."""
    current_app.logger.info("get winners")
    context = ctx.build_context(current_app, ctx.ContextReq.USERS | ctx.ContextReq.LEADERBOARD, leaderboard_groupid=None)
    winners = context["leaderboard"][:10]
    for place, winner in enumerate(winners):
        userid = winner["userid"]
        user = context["users"][userid]
        current_app.logger.info("=======================")
        current_app.logger.info("Platz %d:", place + 1)
        current_app.logger.info("  Punkte: %s", winner["total"])
        current_app.logger.info("  Name: %s", user["username"])
        current_app.logger.info("  E-Mail: %s", user["email"])
        current_app.logger.info("  UserID: %s", userid)
        current_app.logger.info("=======================")


@click.command('test-aoc-py')
def test_aoc_py_command():
    """testing commands"""
    current_app.logger.info("testing get_puzzle")
    current_app.logger.debug(get_puzzle(current_app.config["YEAR"], 20))


def init_app(app):
    """
    Gets called by __init__.py on initialization of the flask App.
    """
    app.cli.add_command(test_aoc_py_command)
    app.cli.add_command(get_winners_command)


def get_aoc_day(timestamp: datetime | None = None, year: int | None = None) -> Tuple[int, int]:
    """
    Returns a tuple:
    - The current AOC Day (1-25), or 0 if the event hasn't started yet, or 26 if the event is already over.
    - The hour (0-23) of the AOC Day of the timestamp.

    Parameters:
    timestamp -- Overwrite the "current" timestamp. Must be timezone aware. If timestamp is None, the current time is used.
    year -- Overwrite the AOC year. If None the YEAR from the config is used.
    """
    if timestamp is None:
        timestamp = datetime.now(tz=timezone.utc)
    timestamp = timestamp.astimezone(ZoneInfo("EST"))  # convert to EST time
    hour = timestamp.hour
    if year is None:
        year = cast(int, current_app.config["YEAR"])
    if timestamp.year > year:
        return 26, hour
    if timestamp.year < year:
        return 0, hour
    if timestamp.month < 12:
        return 0, hour
    return min(timestamp.day, 26), hour


def get_aoc_submission_seconds(timestamp: datetime) -> int:
    """
    Get the number of seconds since the start of the AOC Day a submission was made in.

    This function is useful for tiebreaking.

    Parameters:
    timestamp -- The timestamp of the submission. Must be timezone aware.
    """
    timestamp = timestamp.astimezone(ZoneInfo("EST"))  # convert to EST time
    daystart = datetime(year=timestamp.year, month=timestamp.month, day=timestamp.day, hour=0, minute=0, second=0, tzinfo=timestamp.tzinfo)
    return (timestamp - daystart).seconds
