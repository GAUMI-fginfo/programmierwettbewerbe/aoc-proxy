from typing import Any, Callable, List
import unicodedata as uni

from werkzeug.exceptions import BadRequest
from werkzeug.wrappers import Response

allowed_hex_lower = set("0123456789abcdef")

allowed_name_unicode_general_categories = set([
    # See https://www.unicode.org/reports/tr44/#General_Category_Values
    # See https://www.compart.com/en/unicode/category

    # Abbr   | Long                   | Description
    "Lu"   ,# Uppercase_Letter       | an uppercase letter
    "Ll"   ,# Lowercase_Letter       | a lowercase letter
    "Lt"   ,# Titlecase_Letter       | a digraph encoded as a single character, with first part uppercase
    "LC"   ,# Cased_Letter           | Lu | Ll | Lt
    "Lm"   ,# Modifier_Letter        | a modifier letter
    "Lo"   ,# Other_Letter           | other letters, including syllables and ideographs
    "L"    ,# Letter                 | Lu | Ll | Lt | Lm | Lo
    "Mn"   ,# Nonspacing_Mark        | a nonspacing combining mark (zero advance width
    "Mc"   ,# Spacing_Mark           | a spacing combining mark (positive advance width
    #"Me"   ,# Enclosing_Mark         | an enclosing combining mark
    #"M"    ,# Mark                   | Mn | Mc | Me
    "Nd"   ,# Decimal_Number         | a decimal digit
    "Nl"   ,# Letter_Number          | a letterlike numeric character
    "No"   ,# Other_Number           | a numeric character of other type
    "N"    ,# Number                 | Nd | Nl | No
    "Pc"   ,# Connector_Punctuation  | a connecting punctuation mark, like a tie
    "Pd"   ,# Dash_Punctuation       | a dash or hyphen punctuation mark
    "Ps"   ,# Open_Punctuation       | an opening punctuation mark (of a pair
    "Pe"   ,# Close_Punctuation      | a closing punctuation mark (of a pair
    "Pi"   ,# Initial_Punctuation    | an initial quotation mark
    "Pf"   ,# Final_Punctuation      | a final quotation mark
    "Po"   ,# Other_Punctuation      | a punctuation mark of other type
    "P"    ,# Punctuation            | Pc | Pd | Ps | Pe | Pi | Pf | Po
    "Sm"   ,# Math_Symbol            | a symbol of mathematical use
    "Sc"   ,# Currency_Symbol        | a currency sign
    "Sk"   ,# Modifier_Symbol        | a non-letterlike modifier symbol
    "So"   ,# Other_Symbol           | a symbol of other type
    "S"    ,# Symbol                 | Sm | Sc | Sk | So
    #"Zs"   ,# Space_Separator        | a space character (of various non-zero widths
    #"Zl"   ,# Line_Separator         | U+2028 LINE SEPARATOR only
    #"Zp"   ,# Paragraph_Separator    | U+2029 PARAGRAPH SEPARATOR only
    #"Z"    ,# Separator              | Zs | Zl | Zp
    #"Cc"   ,# Control                | a C0 or C1 control code
    #"Cf"   ,# Format                 | a format control character
    #"Cs"   ,# Surrogate              | a surrogate code point
    #"Co"   ,# Private_Use            | a private-use character
    #"Cn"   ,# Unassigned             | a reserved unassigned code point or a noncharacter
    #"C"     # Other                  | Cc | Cf | Cs | Co | Cn
])
forbidden_name_characters = set([
    "卐",
    "卍",
    "✙",
    "࿕",
    "࿖",
    "ꖦ",
    "࿗",
    "࿘"
])


class ValidationError(BadRequest):
    def __init__(self, *args, response: Response | None = None, sep: str = ' '):
        """Create a ValidationError.

        Arguments:
        *args        -- The description, given as a list of arguments, will be joined by the seperator and used as the description in BadRequest.
        response     -- Will be given to BadRequest.
        sep          -- The seperator for seperating the description elements.
        """
        description = None
        if args is not None and len(args) > 0:
            description = ""
            for i, arg in enumerate(args):
                description += sep if i > 0 else ""
                description += str(arg)
        super().__init__(description=description, response=response)


def contains_only(test: str, allowed: set[str]) -> bool:
    """Test whether a test string contains ONLY the allowed characters.
    """
    return all(c in allowed for c in test)


def validate_userid(rawuserid: Any) -> str:
    """Validate a userid based on its format.

    Returns the validated userid.

    Example: cc268fc1e73a

    Throws ValidationError if the userid is invalid.
    """
    try:
        userid: str = str(rawuserid)
    except:
        raise ValidationError("userid must be a string")
    if len(userid) != 12:
        raise ValidationError("userid must be exactly 12 chars")
    if not contains_only(userid, allowed_hex_lower):
        raise ValidationError("userid must only contain hex chars")
    return userid


def validate_day(rawday: Any) -> int:
    """Validate a day based on its format.

    Returns the valid day.

    Example: 3

    Throws ValidationError if the day is invalid.
    """
    try:
        day: int = int(rawday)
    except:
        raise ValidationError("day must be an int")
    if not (day >= 1 and day <= 25):
        raise ValidationError("day must be between 1 and 25")
    return day


def validate_username(rawusername: Any) -> str:
    """Validate a username based on its format.

    Returns the validated username.

    Example: Jake ♥

    Throws ValidationError if the username is invalid.
    """
    try:
        username: str = str(rawusername)
    except:
        raise ValidationError("username must be a string")

    username = uni.normalize("NFC", username)  # See https://www.unicode.org/faq/normalization.html and https://docs.python.org/3/library/unicodedata.html?highlight=unicodedata#unicodedata.normalize
    # username = uni.normalize("NFKC", username)

    username = username.lstrip().rstrip()  # Remove outer spaces

    if not (len(username) >= 2 and len(username) <= 20):
        raise ValidationError("username must be between 2 and 20 characters long and thus not", len(username))
    for c in username:
        if c == "\N{ZERO WIDTH JOINER}":
            continue  # Allow ZERO WIDTH JOINER
        if not c.isprintable():
            raise ValidationError("username contains non printable character:", uni.name(c, "UNKNOWN"))
        cat = uni.category(c)
        if cat not in allowed_name_unicode_general_categories and c != " ":
            raise ValidationError("username contains a character of unicode general category '" + cat + "' which is not allowed:", uni.name(c, "UNKNOWN"))
        if c in forbidden_name_characters:
            raise ValidationError("username contains a forbidden character:", uni.name(c, "UNKNOWN"))
    if username.startswith("\N{ZERO WIDTH JOINER}") or username.endswith("\N{ZERO WIDTH JOINER}"):
        raise ValidationError("username is not allowed to start or end with ZERO WIDTH JOINER")
    # TODO: Maybe somehow disable right-to-left text? Though technically this isn't the correct place, as we may have arabic users.
    return username


def generate_allowed_username_chars(filename: str = "allowed_username_chars.txt", breakafter: int = 60) -> List[str]:
    allowed = set()
    for i in range(1114111 + 1):  # https://docs.python.org/3/library/functions.html#chr
        c = chr(i)
        try:
            un = validate_username("a" + c)
            allowed.add(un[1:])
        except:
            pass
    allowed = list(allowed)
    allowed.sort()
    print("There are currently", len(allowed), "different characters allowed in a username.")
    with open(filename, "w") as outfile:
        for i, c in enumerate(allowed):
            outfile.write(c)
            if i % breakafter == 0:
                outfile.write("\n")
            else:
                outfile.write(" ")
        outfile.write("\n")

    return allowed


def validate_color(rawcolor: Any) -> str:
    """Validate a color based on its format.

    Returns the validated color as a hex string.

    Example: #1ab2d0f

    Throws ValidationError if the color is invalid.
    """
    try:
        color: str = str(rawcolor)
    except:
        raise ValidationError("color must be a string")
    color = color.lower()  # Hex strings are in lowercase
    if len(color) != (1 + 6):
        raise ValidationError("color must be exactly 7 chars")
    if color[0] != "#":
        raise ValidationError("color must begin with a '#'")
    if not contains_only(color[1:], allowed_hex_lower):
        raise ValidationError("color must only contain hex chars")
    return color


def validate_groupid_or_empty(rawgroupid: Any) -> str:
    try:
        groupid: str = str(rawgroupid)
    except:
        raise ValidationError("groupid must be a string")
    groupid = groupid.replace(" ", "_")  # Replace all spaces with _
    groupid = groupid.lstrip("_").rstrip("_")  # Remove outer spaces
    if groupid == "":
        return groupid
    else:
        return validate_groupid(groupid)


def validate_groupid(rawgroupid: Any) -> str:
    """Validate a groupid based on its format.

    Returns the validated groupid.

    Example: FG Info

    Throws ValidationError if the groupid is invalid.
    """
    try:
        groupid: str = str(rawgroupid)
    except:
        raise ValidationError("groupid must be a string")

    groupid = groupid.replace(" ", "_")  # Replace all spaces with _
    groupid = groupid.lstrip("_").rstrip("_")  # Remove outer spaces
    if not (len(groupid) >= 2 and len(groupid) <= 10):
        raise ValidationError("groupid must be between 2 and 10 characters long and thus not", len(groupid))
    if not groupid.isascii():
        # raise ValidationError("groupid must be ASCII")
        raise ValidationError("Gruppennamen dürfen nur ASCII-Zeichen beinhalten")
    if not groupid.isprintable():
        raise ValidationError("groupid must be printable")
    for c in groupid:
        if not (c.isalnum() or c == "_"):
            raise ValidationError("Zeichen in Gruppennamen dürfen nur entweder alphanumerisch sein oder '_'. Das Zeichen", uni.name(c, "UNKNOWN"), "ist also nicht valide.")
    return groupid


def validate_submission_value(rawsubmission_value: Any) -> str:
    """Validate a submission_value based on its format.

    Returns the validated submission_value.

    Example: FG Info

    Throws ValidationError if the submission_value is invalid.
    """
    try:
        submission_value: str = str(rawsubmission_value)
    except:
        raise ValidationError("submission_value must be a string")

    submission_value = submission_value.lstrip().rstrip()  # Remove outer spaces

    if not (len(submission_value) >= 1 and len(submission_value) <= 1000):
        raise ValidationError("submission_value must be between 1 and 1000 characters long and thus not", len(submission_value))
    if not submission_value.isascii():
        raise ValidationError("submission_value must be ASCII")
    if not submission_value.isprintable():
        raise ValidationError("submission_value must be printable")
    for c in submission_value:
        if not (c.isalnum() or c in [","]):
            raise ValidationError("submission_value chars must be alphanumeric or a comma. Offending character: ", uni.name(c, "UNKNOWN"))
    return submission_value


def test_validate_fn(fnname: str, fn: Callable, valid: list, invalid: list):
    print("Testing:", fnname)
    for i, t in enumerate(valid):
        try:
            fn(t)
        except ValidationError as e:
            raise Exception("Test", i, "for " + fnname + " failed eventhough it should have passed:", t, e)
    for i, t in enumerate(invalid):
        try:
            res = fn(t)
            raise Exception("Test", i, "for " + fnname + " passed eventhough it should have failed:", t, res)
        except ValidationError:
            pass
    print(f"All ({len(valid)}+{len(invalid)}=){len(valid) + len(invalid)} tests for {fnname} passed.")


if __name__ == "__main__":
    if uni.unidata_version != "13.0.0":
        raise Exception("Unicode database version is not 13.0.0:", uni.unidata_version)

    # Test validate_submission_value
    valid = [
        "FGInfo",
        "Info  ","Informatik  ",
        "Bio  ","Biologie  ",
        "BioChem "," Biochemie  ",
        "Phy  ","Physik      ",
        "Math  ","Mathe      ",
        "Chem  ","Chemie      ",
        "Geo  ","Geografie  ",
        "DS      ","DataSci ",
        "Win  ","Windows      ",
        "Linux  ","Linux      ",
        "Mac  ","Mac          ",
        "Spezi  ","Spezi      ",
        "Mate  ","ClubMate  ",
        "Vim  ","Vim          ",
        "Emacs  ","Emacs      ",
        "Nano  ","Nano",
        "Info","Informatik",
        "Bio","Biologie",
        "BioChem","Biochemie",
        "Phy","Physik",
        "Math","Mathe",
        "Chem","Chemie",
        "Geo","Geografie",
        "DS","DataSci",
        "Win","Windows",
        "Linux","Linux",
        "Mac","Mac",
        "Spezi","Spezi",
        "Mate","ClubMate",
        "Vim","Vim",
        "Emacs","Emacs",
        "Nano","Nano",
        "Windows7",
        "aa",
        "aaaaaaaaaa",
        "DataSci",
        "a",
        "0",
        "1",
        "2",
        "3",
        "4",
        "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25",
        "000000",
        "00ff00",
        "00FF00",
        "123456",
        "123456",
        "aaaaaaaaaaa",
        "2,3,4,5,6,7"
    ]
    invalid = [
        "" # TODO Add more tests for invalid submission_values.
    ]
    test_validate_fn("submission_value", validate_submission_value, valid, invalid)

    # Test validate_groupid
    valid = [
        "FG Info",
        "Info  ","Informatik  ",
        "Bio  ","Biologie  ",
        "BioChem "," Biochemie  ",
        "Phy  ","Physik      ",
        "Math  ","Mathe      ",
        "Chem  ","Chemie      ",
        "Geo  ","Geografie  ",
        "DS      ","Data Sci ",
        "Win  ","Windows      ",
        "Linux  ","Linux      ",
        "Mac  ","Mac          ",
        "Spezi  ","Spezi      ",
        "Mate  ","Club Mate  ",
        "Vim  ","Vim          ",
        "Emacs  ","Emacs      ",
        "Nano  ","Nano",
        "Info","Informatik",
        "Bio","Biologie",
        "BioChem","Biochemie",
        "Phy","Physik",
        "Math","Mathe",
        "Chem","Chemie",
        "Geo","Geografie",
        "DS","DataSci",
        "Win","Windows",
        "Linux","Linux",
        "Mac","Mac",
        "Spezi","Spezi",
        "Mate","ClubMate",
        "Vim","Vim",
        "Emacs","Emacs",
        "Nano","Nano",
        "Windows7",
        "aa",
        "aaaaaaaaaa",
        "Data_Sci",
        "Data _Sci",
    ]
    invalid = [
        ""
        "a",
        "aaaaaaaaaaa",
        "Info 	","Informatik 	",
        "Bio 	","Biologie 	",
        "	Biochemie 	",
        "Phy 	","Physik 	    ",
        "Math 	","Mathe 	    ",
        "Chem 	","Chemie 	    ",
        "Geo 	","Geografie 	",
        "DS 	    ",
        "Win 	","Windows 	    ",
        "Linux 	","Linux 	    ",
        "Mac 	","Mac 	        ",
        "Spezi 	","Spezi 	    ",
        "Mate 	","Club Mate 	",
        "Vim 	","Vim 	        ",
        "Emacs 	","Emacs 	    ",
        "Nano 	",
    ]
    test_validate_fn("groupid", validate_groupid, valid, invalid)

    # Test validate_color
    valid = [
        "#000000",
        "#00ff00",
        "#00FF00",
        "#123456",
        "#123456",
    ]
    invalid = [
        "green"
        "1123456",
        "b123456",
        "",
    ]
    test_validate_fn("color", validate_color, valid, invalid)


    # Test validate_userid
    valid = [
        "0123456789ab",
        "01cdef6789ab",
        "381718823298",
        "000000000000",
        "111111111111",
        "000000000001",
        "aaaaaaaaaaaa",
        "bbbbbbbbbbbb",
        "cccccccccccc",
        "dddddddddddd",
        "eeeeeeeeeeee",
        "ffffffffffff",
        "198273981239",
        "acbadefbacde",
        "acb123efb812",
    ]
    invalid = [
        "",
        "0",
        "1",
        "ad1",
        "acb123efb82",
        "gggggggggggg",
        "acb123efb8212",
        "11111111111111",
        " acb123efb812",
        " acb123efb812 ",
        "acb123efb812 ",
        " cb123efb812",
    ]
    test_validate_fn("userid", validate_userid, valid, invalid)


    # Test validate_day
    valid = [
        1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,
        "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25",
        1.5,
        1.0,
    ]
    invalid = [
        26,
        27,
        28,
        112000,
        0,
        -1,
        -2,
        -3,
        -4,
        -24,
        -25,
        -26,
        -27,
        -28,
        -1.0,
        -1.5,
        -5.5,
    ]
    test_validate_fn("day", validate_day, valid, invalid)

    # Test validate_username
    #generate_allowed_username_chars()
    valid = [
        "Jake",
        "Jake ",
        "                                  Jake ",
        "                                Jake                                ",
        "Jake                                   ",
        "Jake ♥",
        "äöüißs",
        "!\"§$%&/()=",
        "fZ88!40(mh!.40/V#L^_",
        "H58Dq;s_KNTQ)'dc7PpY",
        "Xt&a3KQ`-7CpF~r/<;@M",
        "sBfLSk,+*Zv!:?x5zK~m",
        "M$5E`x;Z{r-Lc=vFykzJ",
        "w#>W}d]y^?[,vE+4e)nQ",
        "dM5[CY=-)6c{W#q:Z;bp",
        "f'V9Wy}Y5\"UL;SaXqK7{",
        "Eg&[9#\"V=P36Sk~N@t$^",
        "Ak*[fnjsgbZ5H`M_$E9u",
        "f&B7x;%8yFrN5$)zL<Zw",
        "<<>",
        "ｱｲｳｴｵ",
        "ｱｲｳｴｵ",
        "ｱｲｳｴｵ",
        "アイウエオ",
        "アイウエオ",
        "パピプペポ",
        "パピプペポ",
        "パピプペポ",
        "パピプペポ",
        "パピプペポa",
        "ﾊﾟﾋﾟﾌﾟﾍﾟﾎﾟb",
        "ﾊﾟﾋﾟﾌﾟﾍﾟﾎﾟc",
        "ﾊﾟﾋﾟﾌﾟﾍﾟﾎﾟ",
        "パピプペポ",
        "パピプペポ",
        "ａｂｃＡＢＣ",
        "ａｂｃＡＢＣ",
        "ａｂｃＡＢＣ",
        "abcABC",
        "abcABC",
        "１２３",
        "１２３",
        "１２３",
        "123",
        "123",
        "＋－．～）｝",
        "＋－．～）｝",
        "＋－．～）｝",
        "+-.~)}",
        "+-.~)}",
        "ʰaa",
        # The following line was generated from https://en.wikipedia.org/wiki/List_of_most_popular_given_names
        "도윤","Aada","Aadhya","Aarav","Aariz","Aaron","Aarya","Aasha","Abbas","Abd","Abdallah","Abdelkader","Abdel-Rahman","Abdoulaye","Abdul","Abdulaziz","Abdulla/Abdullah","Abdullah","Abdullah عبدالله","Abdullo","Abdulrahman","Abd عبد","Abubakr","AbulFazl","Adam","Ádám","Adama","Adam آدم","Adel","Adéla","Adin","Aditi","Adomas","Adriana","Adrians","Afonso","Agnes","Agustín","Agustina","Ahmad","Ahmad/Ahmed","Ahmed","Ahmed  أحمد","Ahmet","Aïcha","Aiden","Aikaterini","Aimar","Aino","Aisha","Aisultan","Aisyah","Aitana","Aiur","Ajda","Ajla","Aksel","Alanoud","Albert","Aldiyar","Alejandro","Aleks","Aleksandar","Aleksandar","Aleksander","Aleksandr","Aleksandra","Aleksandra","Aleksandre","ალექსანდრე","Alessandro","Alessia","Àlex","Alexander","Alexander","Alexandra","Alexandra","Alexandru","Alexey","Alfie","Ali","Ali","Ali Asaf","Alice","Alicja","Alikhan","Aline","Alinur","Ali-Reza","Alisa","Alisa","Alise","Aliya","Ali علي","Alma","Alparslan","Alteo","Althea","Álvaro","Alysha","Amahle","Amaia","Amal","Amálie","Amar","Amara","Amaris","Amayra","Ámbar","Ambra","Ambre","Amelia","Amelija","Amēlija","Amelja","Amina","Amina","Aminah","Aminata","Amine","Amir","Amir","Amira","Amir-Abbas","Amir-Ali","Amir-Hossein","Ammar","Amy","An","Ana","Ana","Anahit","Ana Maria","Anar","Anastasia","Anastasia","Anastasia ანასტასია","Anastasija","Anastasiya","Ana ანა","Andrea","Andreas","Andreea","Andrei","Andrej","Andria ანდრია","Andriy","Androula","Ane","Angel","Ángel","Angela","Angeliki","Angelina","Angeline","Angelo","Anna","Anna","Annie","Anohito","Antonella","Antoni","Antônia","Antonio","Antônio","Anzu 杏","Ao","Aoi","Aoife","Aoi 碧","Ao 蒼","Aputsiaq","Aras","Archie","Areg","Areni","Ari","Aria","Ariana","Arianna","Ariel","Arjun","Arlo","Arman","Armen","Armine","Aron","Arpi","Artem","Arthur","Artiom","Artur","Artyom","Aruuke","Aruuzat","Asel","Asemahle","Ashot","Ashraqat","Asilim","Asiya","Asja","Asmita","Assia","Astrid","Asya","Athanasios","Atlas","August","Aurora","Ava","Aviana","Avigail","Awa","Aya","Ayala","Ayala","Ayalah","Ayesha","Aygun","Ayim","Ayla","Aylin","Aylin","Ayoub","Ayra","Ayşa","Ayşe","Aysel","Ayzere","Azaria","Azariah","Aziz","Azora Kiara","Azra","Aþena","Bader","Bakary","Baldur","Bandar","Banele","Bára","Baran","Barbara","Barbare  ბარბარე","Bartal","Basiliki","Batkhaаn","Bautista","Bayarmaa","Baаtar","Beatrice","Beatrice","Beatrix","Beatrise","Beatriz","Ben","Benas","Bence","Benedita","Benicio","Benjamin","Benjamín","Bernardo","Beshoi","Bibek","Biel","Bilal","Bilal","Biljana","Binita","Bintou","Bisera","Bishal","Bismah","Bjarta","Bjørg","Bogdan","Bogdana","Boglárka","Bolormaa","Boris","Brahim","Brandur","Bruno","Camila","Camille","Carla","Carlos","Carmen","Carolina","Catalina","Cecília","Celeste","Celine","César","Charbel","Charles","Charlie","Charlotte","Chedeline","Chia-hao","Chien-hung","Chih-chiang","Chih-hao","Chih-ming","Chih-wei","Chin-lung","Chloe","Chloé","Christian","Christina","Christina","Christos","Chuluun","Chun-chieh","Chun-hung","Cillian","Ciro","Clara","Concepción","Daisy","Dalal","Dalila","Damian","Damjan","Dan","Danel","Daniel","Daniel","Daniela","Daniel Alejandro","Daniels","Daniel დანიელ","Daniil","Danijel","Danna","Danylo","Daria","Daria","Darija","Daris","Dārta","Davi","David","David","Davit","Davit დავითი","Davud","Defne","Dejan","Dejan","Delfina","Demetre დემეტრე","Dhia","Dhruv","Diego","Diego Alejandro","Dimitar","Dimitra","Dimitrios","Dion","Djamel","Djeneba","Dmitry","Dmitry","Dmytro","Doha","Dominik","Dominykas","Dora","Dorothy","Do-yun","Dragan","Dragan","Dragana","Duarte","Dunja","Dušan","Dylan","Dylan/Dyllan","Džan","Éabha","Ecrin","Eden","Edoardo","Édouard","Edward","Eeli","Eevi","Eino","Eisha","Eitan","Eithan","Elchin","Elena","Elene ელენე","Eleni","Elias","Elias/Elyas","Elie","Elif","Elija","Elijah","Eliška","Elizabeth","Elizaveta","Ella","Ellen","Elli","Ellie","Elmar","Elnur","Elshan","Elvira","Elza","Ema","Eman","Ema 咲茉","Embla","Emil","Emilia","Emilía","Emilia","Emelia","Emiliya","Emiliano","Emilija","Emīls","Emily","Emily/Emely","Emine","Emir","Emma","Emmanuel","Ernawati","Esma","Esperanza","Esther","Ethan","Eun-woo","Eva","Eva","Evelina","Evelyn","Evens","Evgeny","Evie","Eylül","Eymen","Ezekiel","Ezequiel","Fadi","Fahad","Fahd","Fahd فهد","Faisal","Fajr","Fanta","Farah","Farah فرح","Farida","Farrah","Farzana","Fatemeh","Fatemeh-Zahra","Fatima","Fátima","Fatima/Fatma","Fatima فاطمة","Fatin","Fatma","Fatoumata","Felipe","Felix","Fernanda","Fernando","Fiadh","Filip","Finlay","Finley","Finn","Florence","Fozia","Fran","Francesco","Francisca","Francisco","Franciszek","Freddie","Frederick","Freja","Freya","Frida","Frida/Frieda","Fūma 颯真","Gabija","Gabor მათე","Gabriel","Gabriela","Gabriela","Gabriele","Gabriel გაბრიელი","Gael","Gagik","Gamalat","Gamila","Gaspar","Gayane","George","Georgi","Georgia","Georgia (Giorgia","Georgios","Gerónimo","Gevorg","Gianna","Gilli","Ginevra","Giorgi გიორგი","Gita","Giulia","Gogo)","Göktug","Googoosh","Goran","Goran","Gordana","Grace","Guadalupe","Guðrún","Gunay","Gunel","Gustavs","Habib","Habiba","Hadicha","Ha-eun (하은)","Ha-joon (하준)","Halim","Hamza","Hana","Hana","Hanna","Hannah","Hàorán 浩然","Hàoyǔ 浩宇","Ha-rin (하린)","Harper","Harraz","Harris"," Harry","Harry","Haruma 悠真","Harun","Haruna 陽菜","Haruto","Haru 暖","Hasan","Hashem","Hasmik","Hasnaa","Hassan","Hatice","Hawa","Hayk","Ha-yoon (하윤)","Heikapu","Heitor","Helena","Helma","Helmi","Henry","Herkus","Herman","Hessa","Himari","Hina","Hinano","Hinata","Hinato 陽翔","Hina 陽葵","Hiro","Hoda","Hosna","Hosniya","Hossein","Hovhannes","Hudson","Hugo","Hur حور","Hüseyin","Huseyn","Hussa","Hussain","Hussein","Hydar","Ian","Ibai","Iben","Ibrahim","I-chun","Ida","Igor","Iker","Ilgar","Ilija","Iman","Imene","Iminathi","Inaya","İnci","Indira","Inès","Inuk","Inunnguag","Inutsiaq","Ioane","Ioanna","Ioannis","Ion","Irati","Irene","Irina","Isaac","Isabel","Isabella","Isabella/Isabelle","Isabelle/Isabella","Isak","Isidora","Isla","Islande","Ismail","İsmail","Itsuki","Ivaana","Ivalu","Ivan","Ivan","Ivana","Ivana","Ivik","Ivy","Izaro","Izz","Jack","Jackson","Jacob","Jad","Jade","Jakob","Jakov","Jakov","Jakub","James","Jameson","Jan","Jana","Jana","Javier","Jēkabs","Jelena","Jennifer","Jerónimo","Jessica","Ji-a","Ji-an","Ji-ho (지호)","Ji-woo","Jiya","Ji-yoo","João","Joe","Joel","John","Jokūbas","Jökull","Jon","Jón","Jonas","Jordi","Jorge","Jori","José","Josefa","José Luis","Joseph","Joshua","Josiah","Josip","Jouri","Jovan","Jovan","Jovana","Juan","Juana","Juan Andrés","Juan Carlos","Jude","Judeline","Julen","Jules","Julia","Júlia","Juliana","Julie","Julieta","Juliette","Julija","Junaidi","June","Junior","Ju-won","Kabir","Kabita","Kadiatou","Kai","Kaia","Kairo","Kajus","Kaloyan","Kamal","Kamilė","Karam","Karen","Kári","Karim","Karima","Karine","Karl","Kārlis","Karolína","Kartini","Katerina","Katerina","Kathem","Katla","Kazi","Keanu","Kevin/Keven","Khadije","Khaled","Khaled خالد","Khawla","Khongordzol","Kiaan","Kiara","Kiran","Kirollos","Kiwa","Klea","Koa","Konstantina","Konstantinos","Konul","Kora","Kostantina","Krishna","Kristian","Ksenija","Kyra","Kyriaki","Laia","Lamija","Lan","Lana","Lara","Latifa","Laura","Lavi","Layla","Lazar","Lea","Léa","Leah","Leandra","Leano","Leen","Leevi","Lėja","Lejla","Lena","Léna","Leni","Lenna","Leo","Léo","Leon","Leonardo","Leonor","Lesedi","Lethabo","Lethokuhle","Lev","Lev","Levente","Levi","Lewis","Leyla","Lia","Lía","Liam","Lian","Lianne","Lian ليان","Libi","Lidia","Liepa","Li-hua","Lile ლილე","Lili","Lilit","Lilja","Lilly","Lily","Lily/Lillie/Lilly","Lin","Lina","Linda","Lisakhanya","Liv","Livia","Liz","Ljiljana","Lorenzo","Louis","Louise","Louis/Luis","Lourenço","Lovelie/Lovely","Lovro","Luana","Lubanzi","Luca","Lucas","Lucia","Lucía","Luciana","Lucija","Ludovica","Luis","Luiz","Luka","Luka","Lukas","Lukáš","Luka ლუკა","Luknė","Lulwa","Luna","Lur","Lusine","Lwandle","Lydia","Lyn","Lyna","Lyn لين","Mabel","Maddi","Mädïna","Maël","Maeva","Maha","Mahamadou","Mahammad","Mahdi","Mahesh","Mahmoud","Maia","Maitê","Maja","Maksim","Maksym","Malachi","Malak","Malak ملك","Malea","Malek","Malen","Maligiaq","Malik","Malthe","Malu","Mamadou","Manaia","Maneh","Manish","Manua","Manuel","Manuela","Marama"," Marc","Marc","Marcell","Marcia","Marco Antonio","Marcos","Mareva","Margaret","Margarida","Mari","Maria","Maria","María","Maria Alice","María del Carmen","María Fernanda","María José","Mariam","Mariami მარიამი","Mariana","María Teresa","Maria ماريا","Marie","Marija","Marija","Marina","Mario","Marios","Mariya","Mariya","Mark","Mark","Markas","Markel","Marko","Marks","Marta","Martha","Marti","Martí","Martim","Martin","Martin","Martín","Martina","Marwa","Mary","Marya","Maryam","Maryam مريم","Maryja","Maša","Ma'soumeh","Matas","Máté","Matei","Matej","Matej","Matěj","Mateo","Mathias","Matías","Matija","Matilda","Matilde","Matteo","Matthew","Matthías","Matthildur","Mattia","Mattias","Matvey","Matvey","Matyáš","Maui","Max","Maxim","Maxim","Maximilian","Maximiliano","Maya","Maysoun","Mees","Mehar","Mehdi","Mehmet","Mei-hui","Mei-ling","Mei 芽依","Melina","Melisa","Melissa","Melokuhle","Meriem","Merjem","Mersana","Meryem","Mia","Mía","Michael","Michail","Michail","Michal","Michalis","Miguel","Miguel Ángel","Mihail","Mikael","Mikaere","Mikayel","Mikhail","Mikhail","Miki","Mikołaj","Mila","Mila","Milagrosa","Milan","Milan","Milán","Milana","Mila ميلا","Milena","Milena","Milica","Millie","Miloš","Mina","Minato 湊","Míngzé 茗泽","Minik","Min-jun","Mio","Mira","Miraç","Miriam","Mirjana","Mirlande/Myrlande","Moana","Modibo","Moea","Moeata","Mohamed","Mohamed محمد","Mohammad","Mohammad-Reza","Mohammad-Taha","Mohammed","Mona","Monte","Moshe","Moussa","Mùchén 沐宸","Mùchén 沐辰","Muhamad","Muhammad","Muhammad","Muhammed","Muhammed","Mulyadi","Murad","Mustafa","Mùyáng 沐阳","Mykhaylo","NA","Nada","Nagi","Nagisa 凪","Nahia","Naïm","Naranbaatar","Nare","Narek","Narine","Natalia","Natália","Natálie","Natalija","Nathalie","Nathan","Nathaniel","Naura","Naya","Nazanin-Zahra","Nehir","Nela","Nelio Raphael/Rafael","Nesreen","Neža","Nia  ნია","Nicolás","Nicole","Niel","Niels","Niharika","Nik","Nika","Nikau","Niko","Nikodem","Nikol","Nikola","Nikola","Nikolaos","Nikoloz ნიკოლოზი","Nil","Nilay","Nils","Nina","Nino  ნინო","Nivi","Niviaq","Noa","Nóa","Noah","Noam","Noel","Noemí","Nomaan","Noor","Nora","Norsaq","Nouf","Noûr","Nuka","Nur","Nuray","Nurhayati","Nurislam","Nurul","Nur نور","Nutsa ნუცა","Odval","Ofentse","Oihan","Oisha","Oisin","Oisín","Oleksandr","Oleksandra","Olga","Oliver","Olivér","Óliver","Olivia","Olívia","Oliwia","Omar","Omar","Omar عمر","Omer","Ömer","Ömer Asaf","Omphile","Ona","Onni","Orla","Oscar","Oskar","Osman","Oumar","Oumou","Oyunbileg","Pablo","Panagiota","Panagiotis","Paneeraq","Paninnguaq","Paraskeui","Paraskevi","Pari","Patricia","Patrícia","Patricija","Pau","Paul","Paula","Paulo","Pedro","Petar","Petar","Peter","Peterson"," Phoebe","Pierre","Pipaluk","Poema","Pol","Pola","Polina","Polina","Prakash","Prasert","Prem","Princess","Puteri","Qillaq","Rabina","Rachid","Radmila","Ragnar","Ralfs","Ramazan","Ramón","Ramona","Rania","Raphael","Raphaël","Raquel","Rashad","Rasmus","Raul","Ravi","Raya","Rayana","Reem","Reema","Regina","Reina","Rei 澪","Relja","Renata","Ren 蓮","Rethabile","Reza","Rían","Ricardo","Riccardo","Richard","Riko 莉子","Rin 凛","Rita","Roan","Robert","Roberts","Robin","Roel","Roghayyeh","Roko","Roman","Ronja","Rory","Rose","Rose-Merline","Rosie","Rosmery","Rowan","Roxana","Rudy","Ruòxī 若汐","Saar","Safija","Safiya","Saga","Sahar","Said","Sakeena","Sakineh","Sakura さくら","Saliha","Salik","Salma","Salomé","Sama","Samantha","Samir","Samuel","Samvel","Samyar","Sandra","Santiago","Saqib","Sara","Sara","Sára","Sarah","Sara/Sarah","Sebastian","Sebastián","Sekou","Selim","Selma","Sem","Seo-ah","Seo-jun","Seo-yun","Sergei","Şerife","Sesili სესილი","Sevda","Sevinj","Shahd","Shahem","Shahid","Shaikha","Shaimaa","Shams","Shayma","Sheikh","Sherifa","Shivansh","Shristi","Shu-chen","Shu-chuan","Shu-fen","Shu-hua","Shu-hui","Siddhartha","Sienna","Simona","Siphosethu","Sita","Siti","Siti Aminah","Si-woo","Slamet","Snezana","Snežana","Sō","Sobia","Sofi","Sofia","Sofia","Sofía","Sofia/Sophia","Sofie","Sofija","Sofija","Sofiya","Sofiya","Sóley","Sólja","Solomiya","Sōma","Somayyeh","Somboon","Somchai","Somporn","Somsak","Sonia","Soo-ah","Sophia","Sophia","Sophie","Souleymane","Sri Wahyuni","Stanisław","Stanley","Stefan","Stefan","Stella","Stevenson","Suha","Sukhbaаtar","Sulastri","Sultan","Sumarni","Sumayah","Sumiati","Sunarti","Sunita","Supardi","Suparman","Suprianto","Susan","Susanna","Sutrisno","Suzana","Syed","Szymon","Tadhg","Taha","Taim","Tamar","Tamatoa","Tapuarii","Tara","Tarana","Tareq","Tarita","Tatiana","Tatsuki 樹","Tehei","Teiki","Teiva","Teo","Teodor","Teodora","Tereza","Tess","Teura","Teva","Theo","Théo","Theodor","Theodore","Theo/Theodore","Thiago","Thomas","Tiago","Tiare","Tigran","Tim","Timofey","Titaina","Titaua","Tobias","Toivo","Tóki","Tom","Tomás","Tomáš","Tommaso","Tommy/Tommie","Tomyris","Tóra","Trinidad","Tuleen","Tunar","Turki","Uiloq","Ulloriaq","Umar","Umar","Uri","Usman","Usman","Uta 詩","Vaea","Väinö","Valdemar","Valentin","Valentín","Valentina","Valeria","Valter","Vamika","Varvara","Varvara","Vasileios","Vasilije","Vasiliki","Vasilisa","Vaso","Vedant","Venla","Vera","Veronika","Vesna","Vesna","Vicente","Victor","Victor Hugo","Victoria","Viktor","Viktor","Viktoria","Viktoría","Viktória","Viktorie","Viktoriya","Viktoriya","Violeta","Viraj","Vita","Vittoria","Vladyslav","Vojtěch","Vugar","Vuk","Vusal","Vusala","Wahyudi","Wan","Wassim","Wateen","Wen-Hsiung","Widelene","William","Willow","Wilson","Ximena","Xīnyí 欣怡","Yael","Yamato 大和","Yara","Yasmin","Yasmine","Yasna","Yassin","Ya-ting","Yehuda","Ye-jun","Yekaterina","Yelena","Yeva","Yìchén 奕辰","Yìhán 艺涵","Yǐmò 苡沫","Yīnuò 一诺","Yīnuò 依诺","Yìzé 奕泽","Yoana","Yosef","Younès","Yousef","Yousouf يوسف","Youssef","Yua 結愛","Yuina","Yu-jun","Yulia","Yūma","Yuna","Yūna 結菜","Yusef","Yusif","Yusuf","Yǔtóng 语桐","Yǔtóng 雨桐","Yǔxī 语汐","Yǔxuān 宇轩","Yǔzé 宇泽","Zahid","Zahir","Zahra","Zainab","Zala","Zalán","Žan","Zehra","Zeinab","Zeynab","Zeynep","Zǐhán 梓涵","Zina","Zion","Zoe","Zoé","Zofia","Zoran","Zoran","Zümra","Zuzanna","Αγγελική","Αθανάσιος","Αικατερίνη","Ανδρέας","Ανδρούλα","Άννα","Βασίλειος","Βασιλική","Γεωργία","Γεώργιος","Δήμητρα","Δημήτριος","Ελένη","Ιωάννα","Ιωάννης","Κατερίνα","Κυριακή","Κωνσταντίνα","Κωνσταντίνος","Μαρία","Μάριος","Μιχαήλ","Μιχάλης","Νικόλαος","Παναγιώτα","Παναγιώτης","Παρασκευή","Σοφία","Χρήστος","Χριστίνα","Χρίστος","Айзере","Айлин","Айсұлтан","Айша","Айым","Алдияр","Александар","Александр","Александра","Александър","Алексей","Али","Алинур","Алинұр","Алиса","Алихан","Алия","Амина","Амир","Ана","Анастасия","Андрей","Анна","Артём","Аруузат","Арууке","Асылым","Аяла","Әли","Билал","Бисера","Борис","Варвара","Весна","Виктор","Виктория","Габриела","Георги","Горан","Гордана","Давид","Дамјан","Даниел","Даниил","Дарија","Дария","Дејан","Димитър","Дмитрий","Драган","Драгана","Душан","Ева","Евгений","Екатерина","Елена","Елизавета","Зоран","Иван","Ивана","Ирина","Йоана","Јана","Јелена","Јован","Јована","Калоян","Лев","Лука","Љиљана","Максим","Марија","Мария","Марк","Марко","Мартин","Матвей","Матеј","Медина","Мила","Милан","Милена","Милица","Милош","Мирјана","Михаил","Мухаммад","Мұхаммед","Наталья","Никол","Никола","Нурислам","Нұрислам","Ольга","Омар","Петар","Полина","Радмила","Рамада́н","Рая","Раяна","Салиха","Сара","Сафия","Сергей","Симона","Снежана","Софија","София","Стефан","Татьяна","Теодор","Томирис","Умар","Усман","Фатима","Хадича","Хана","Эмир","Юлия","俊傑","俊宏","家豪","建宏","志偉","志強","志明","志豪","怡君","文雄","淑娟","淑惠","淑芬","淑華","淑貞","美惠","美玲","金龍","雅婷","麗華",
        "😀 😃 😄 😁 😆 😅 😂 🤣 🥲 ",
        "☺️ 😊 😇 🙂 🙃 😉 😌 😍 🥰 😘",
        "😗 😙 😚 😋 😛 😝 😜 🤪 🤨 🧐 ",
        "🤓 😎 🥸 🤩 🥳 😏 😒 😞 😔 😟",
        "😕 🙁 ☹️ 😣 😖 😫 😩 🥺 😢 😭  ",
        "😮💨 😤 😠 😡 🤬 🤯 😳 🥵",
        "🥶 😱 😨 😰 😥 😓  🤗  🤔 ",
        " 🤭 🤫 🤥 😶 😶🌫️ 😐 😑",
        "😬 🙄 😯 😦 😧 😮 😲 🥱 ",
        "😴 🤤 😪 😵 😵‍💫  🤐 🥴",
        "🤢 🤮 🤧 😷 🤒 🤕 🤑 🤠 😈 👿 ",
        "👹 👺 🤡 💩 👻 💀 ☠️ 👽 👾 🤖",
        "🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 ",
        "👋 🤚 🖐 ✋ 🖖 👌 🤌 🤏 ✌️ 🤞",
        "a🥲",
        "🥸Verkleidet"
        "a🤌",
        "🤌🏻",
        "🤌🏼",
        "🤌🏽",
        "🤌🏾",
        "🤌🏿",
        "a🫀",
        "🫁Lunge"
        "🥷Ninja"
        "🥷🏻Ninja",
        "🥷🏼Ninja",
        "🥷🏽Ninja",
        "🥷🏾Ninja",
        "🥷🏿Ninja",
        "🤵‍♂️",
        "🤵🏻‍♂️",
        "🤵🏼‍♂️",
        "🤵🏽‍♂️",
        "🤵🏾‍♂️",
        "🤵🏿‍♂️",
        "🤵‍♀️",
        "🤵🏻‍♀️",
        "🤵🏼‍♀️",
        "🤵🏽‍♀️",
        "🤵🏾‍♀️",
        "🤵🏿‍♀️",
        "👰‍♂️",
        "👰🏻‍♂️",
        "👰🏼‍♂️",
        "👰🏽‍♂️",
        "👰🏾‍♂️",
        "👰🏿‍♂️",
        "👰‍♀️",
        "👰🏻‍♀️",
        "👰🏼‍♀️",
        "👰🏽‍♀️",
        "👰🏾‍♀️",
        "👰🏿‍♀️",
        "👩‍🍼",
        "👩🏻‍🍼",
        "👩🏼‍🍼",
        "👩🏽‍🍼",
        "👩🏾‍🍼",
        "👩🏿‍🍼",
        "👨‍🍼",
        "👨🏻‍🍼",
        "👨🏼‍🍼",
        "👨🏽‍🍼",
        "👨🏾‍🍼",
        "👨🏿‍🍼",
        "🧑‍🍼",
        "🧑🏻‍🍼",
        "🧑🏼‍🍼",
        "🧑🏽‍🍼",
        "🧑🏾‍🍼",
        "🧑🏿‍🍼",
        "🧑‍🎄"
        "🧑🏻‍🎄Weihnachts",
        "🧑🏼‍🎄Weihnachts",
        "🧑🏽‍🎄Weihnachts",
        "🧑🏾‍🎄Weihnachts",
        "🧑🏿‍🎄Weihnachts",
        "🫂a",
        "🐈‍⬛",
        "🦬Bison",
        "🦣Mammut",
        "🦫Biber",
        "🐻‍❄️Eisbär",
        "🦤Dodo",
        "🪶Feder",
        "🦭Seehund",
        "🪲Käfer",
        "🪳Kakerlake",
        "🪰Fliege",
        "🪱Wurm",
        "🪴Topfpflanze",
        "🫐Blaubeeren",
        "🫒Olive",
        "🫑Paprika",
        "🫓Fladenbrot",
        "🫔Tamale",
        "🫕Fondue",
        "🫖Teekanne",
        "🧋Bubble Tea",
        "🪨Felsen",
        "🪵Holz",
        "🛖Hütte",
        "🛻Pick-Up",
        "🛼Rollschuh",
        "🪄Zauberstab",
        "🪅Piñata",
        "🪆Matroschka",
        "🪡Nähnadel",
        "🪢Knoten",
        "🩴Zehensandale",
        "🪖Militärhelm",
        "🪗Akkordeon",
        "🪘AfrikanischeTrommel",
        "🪙Münze",
        "🪃Bumerang",
        "🪚Handsäge",
        "🪛Schraubenzieher",
        "🪝Haken",
        "🪜Leiter",
        "🛗Fahrstuhl",
        "🪞Spiegel",
        "🪟Fenster",
        "🪠Saugglocke",
        "🪤Mausefalle",
        "🪣Eimer",
        "🪥Zahnbürste",
        "🪦Grabstein",
        "🪧Protestschild",
        "⚧️Transgender-Symbol",
        "🏳️‍⚧️Transgender",
        " 👈 👉 👆 🖕 👇 ☝️ 👍 👎 ✊",
        "👊 🤛 🤜 👏  🙌 👐 🤲 🤝 🙏 ",
        "✍️ 💅 🤳 💪 🦾 🦵 🦿 🦶 👣 👂",
        "🦻 👃 🫀 🫁 🧠 🦷 🦴 👀 👁 👅 ",
        "👄  💋 🩸👶 👧 🧒 👦 👩 🧑",
        "👨 👩‍🦱 🧑‍🦱 👨   ",
        "♀️ 👱 👱‍♂️ 👩 ",
        "🦳 🧑‍🦳 👨‍🦳 👩‍🦲 🧑‍🦲",
        "👨‍🦲 🧔‍♀️ 🧔 🧔    ",
        "♂️ 👵 🧓 👴 👲 👳‍♀️ 👳 👳",
        "♂️ 🧕 👮‍♀️ 👮 👮‍♂️ 👷",
        "♀️ 👷 👷‍♂️ 💂‍♀️ 💂",
        "💂‍♂️ 🕵️‍♀️ 🕵️ 🕵️     ",
        "♂️ 👩‍⚕️ 🧑‍⚕️ 👨‍⚕️ 👩",
        "🌾 🧑‍🌾 👨‍🌾",
        " 👩‍🍳 🧑‍🍳 👨‍🍳 👩‍🎓 🧑",
        "🎓 👨‍🎓 👩‍🎤",
        " 🧑‍🎤 👨‍🎤 👩‍🏫 🧑‍🏫 👨",
        "🏫 👩‍🏭 🧑‍🏭",
        " 👨‍🏭 👩‍💻 🧑‍💻 👨‍💻 👩",
        "💼 🧑‍💼 👨‍💼",
        " 👩‍🔧 🧑‍🔧 👨‍🔧 👩‍🔬 🧑",
        "🔬 👨‍🔬 👩‍🎨",
        " 🧑‍🎨 👨‍🎨 👩‍🚒 🧑‍🚒 👨",
        "🚒 👩‍✈️ 🧑‍✈️  ",
        "👨‍✈️ 👩‍🚀 🧑‍🚀 👨‍🚀 👩",
        "⚖️ 🧑‍⚖️ 👨‍⚖️ 👰",
        "♀️ 👰 👰‍♂️ 🤵‍♀️ 🤵 🤵",
        "♂️ 👸  🤴 🥷 🦸‍♀️ 🦸 🦸 ",
        "♂️ 🦹‍♀️ 🦹 🦹‍♂️ 🤶",
        "🧑‍🎄 🎅 🧙‍♀️ 🧙 🧙 ",
        "♂️ 🧝‍♀️ 🧝 🧝‍♂️ 🧛",
        "♀️ 🧛 🧛‍♂️ 🧟‍♀️ 🧟 🧟",
        "♂️ 🧞‍♀️ 🧞 🧞‍♂️ 🧜",
        "♀️ 🧜 🧜‍♂️ 🧚‍♀️",
        " 🧚 🧚‍♂️  👼 🤰  🤱 👩",
        "🍼 🧑‍🍼 👨‍🍼 🙇   ",
        "♀️ 🙇 🙇‍♂️ 💁‍♀️ 💁 💁",
        "♂️ 🙅‍♀️ 🙅 🙅‍♂️ 🙆   ",
        "♀️ 🙆 🙆‍♂️ 🙋‍♀️ 🙋 🙋",
        "♂️ 🧏‍♀️ 🧏 🧏‍♂️",
        " 🤦‍♀️ 🤦 🤦‍♂️ 🤷‍♀️ 🤷 🤷",
        "♂️ 🙎‍♀️ 🙎 🙎‍♂️",
        " 🙍‍♀️ 🙍 🙍‍♂️ 💇‍♀️ 💇 💇",
        "♂️ 💆‍♀️ 💆 💆‍♂️",
        "🧖‍♀️🧖🧖‍♂️💅🤳💃🕺👯",
        "♀️👯👯‍♂️🕴👩",
        "🦽🧑‍🦽👨‍🦽👩‍🦼🧑",
        "🦼👨‍🦼🚶‍♀️🚶🚶",
        "♂️👩‍🦯🧑‍🦯👨‍🦯",
        "🧎‍♀️🧎🧎‍♂️🏃",
        "♀️🏃🏃‍♂️🧍‍♀️🧍🧍",
        "♂️👭🧑‍🤝‍🧑👬👫",
        "👩‍❤️‍👩💑👨‍❤️‍👨",
        "👩‍❤️‍👨👩‍❤️",
        "💋‍👩💏👨‍❤️‍💋‍👨",
        "👩‍❤️‍💋‍👨👪",
        "👨‍👩‍👦👨‍👩‍👧👨",
        "👩‍👧‍👦👨‍👩",
        "👦‍👦👨‍👩‍👧‍👧👨",
        "👨‍👦👨‍👨",
        "👧👨‍👨‍👧‍👦👨‍👨‍👦",
        "👦👨‍👨‍👧",
        "👧👩‍👩‍👦👩‍👩‍👧👩",
        "👩‍👧‍👦👩‍👩",
        "👦‍👦👩‍👩‍👧‍👧👨",
        "👦👨‍👦‍👦👨",
        "👧👨‍👧‍👦👨‍👧‍👧👩",
        "👦👩‍👦‍👦👩",
        "👧👩‍👧‍👦👩‍👧‍👧🗣",
        "👤👥🫂🧳🌂☂️🧵🪡🪢🧶",
        "👓🕶🥽🥼🦺👔👕👖🧣",
        "🧤🧥🧦👗👘🥻🩴🩱🩲🩳",
        "👙👚👛👜👝🎒👞👟🥾🥿",
        "👠👡🩰👢👑👒🎩🎓🧢⛑",
        "🪖💄💍💼👋🏻🤚🏻🖐🏻✋🏻🖖🏻👌",
        "🏻🤌🏻🤏🏻✌🏻🤞🏻🏻🤟",
        "🏻👇🏻☝🏻👍🏻👎🏻✊🏻👊",
        "🏻🤛🏻🤜🏻👏🏻🏻🙌🏻👐🏻🤲🏻🙏",
        "🏻✍🏻💅🏻🤳🏻💪🏻🦵",
        "🏻🦶🏻👂🏻🦻🏻👃🏻👶🏻👧",
        "🏻🧒🏻👦🏻👩🏻🧑🏻👨🏻👩🏻‍🦱","🧑🏻‍🦱👨🏻",
        "🦱👩🏻‍🦰🧑🏻‍🦰",
        "👨🏻‍🦰👱🏻‍♀️👱🏻👱🏻‍♂️👩🏻‍🦳",
        "🧑🏻‍🦳👨🏻‍🦳👩🏻",
        "🦲🧑🏻‍🦲👨🏻‍🦲🧔🏻‍♀️🧔🏻🧔",
        "🏻‍♂️👵🏻🧓🏻👴🏻👲🏻",
        "👳🏻‍♀️👳🏻👳🏻‍♂️🧕🏻👮","🏻‍♀️👮🏻👮",
        "🏻‍♂️👷🏻‍♀️👷🏻👷",
        "🏻‍♂️💂🏻‍♀️💂🏻💂🏻♂️🕵🏻‍♀️",
        "🕵🏻🕵🏻‍♂️👩🏻‍⚕️",
        "🧑🏻‍⚕️👨🏻‍⚕️👩🏻‍🌾🧑🏻‍🌾👨",
        "🏻‍🌾👩🏻‍🍳🧑🏻",
        "🖐🏼✋🏼🖖🏼👌🏼🤌🏼🤏🏼✌🏼🤞🏼🏼🤟🏼",
        "🏼👈🏼👉🏼👆🏼🖕🏼👇🏼☝","🏼👍🏼👎🏼✊🏼👊🏼🤛🏼🤜🏼👏",
        "🏼🙌🏼👐🏼🤲🏼🙏🏼✍",
        "♂️🛀🏼🛌🏼👋🏽🤚🏽🖐🏽✋🏽🖖🏽👌🏽🤌",
        "🏽‍♂️🛀🏽🛌🏽👋🏾🤚🏾",
        "🖐🏾✋🏾🖖🏾👌🏾🤌🏾🤏🏾✌🏾🏾🤟🏾🤘🏾🤙",
        "🛀🏾🛌🏾👋🏿🤚🏿🖐🏿✋🏿🖖🏿👌🏿🤌",
        "🏿‍♂️ 🛀🏿 🛌🏿🐶 🐱 🐭 🐹",
        " 🐰 🦊 🐻 🐼 🐻‍❄️ 🐨 🐯 🦁 🐮",
        "🐷 🐽 🐸 🐵 🙈 🙉 🙊 🐒 🐔 🐧 ",
        "🐦 🐦‍⬛ 🐤 🐣 🐥 🦆 🦅 🦉",
        "🦇 🐺 🐗 🐴 🦄 🐝 🪱 🐛 🦋 🐌 ",
        "🐞 🐜 🪰 🪲 🪳 🦟 🦗 🕷 🕸 🦂",
        "🐢 🐍 🦎 🦖 🦕 🐙 🦑 🦐 🦞 🦀 ",
        "🐡 🐠 🐟 🐬 🐳 🐋 🦈 🐊",
        "🐅 🐆 🦓 🦍 🦧 🦣 🐘 🦛 🦏 ",
        "🐪 🐫 🦒 🦘 🦬 🐃 🐂 🐄 🐎 🐖",
        "🐏 🐑 🦙 🐐 🦌 🐕 🐩 🦮 🐕 ",
        "🦺 🐈 🐈‍⬛ 🪶 🐓 🦃",
        "🦤 🦚 🦜 🦢 🦩 🕊 🐇 🦝 🦨 ",
        "🦡 🦫 🦦 🦥 🐁 🐀 🐿 🦔 🐾 🐉",
        "🐲 🌵 🎄 🌲 🌳 🌴 🪵 🌱 ",
        "🌿 ☘️ 🍀 🎍 🪴 🎋 🍃 🍂 🍁 🍄",
        "🐚🪨🌾💐🌷🌹🥀🌺🌸",
        "🌼🌻🌞🌝🌛🌜🌚🌕🌖",
        "🌗🌘🌑🌒🌓🌔🌙🌎🌍🌏",
        "🪐💫⭐️🌟✨⚡️","☄️💥🔥🌪",
        "🌈☀️🌤⛅️🌥☁️🌦🌧⛈🌩🌨",
        "❄️☃️⛄️🌬💨💧💦☔️",
        "☂️🌊🌫🍏🍎🍐🍊🍋🍌🍉🍇",
        "🍓🫐🍈🍒🍑🥭🍍🥥🥝",
        "🍅🍆🥑🥦🥬🥒🌶🫑🌽",
        "🥕🫒🧄🧅🥔🍠🥐🥯",
        "🍞🥖🥨🧀🥚🍳🧈🥞🧇🥓",
        "🥩🍗🍖🦴🌭🍔🍟🍕🫓🥪",
        "🥙🧆🌮🌯🫔🥗🥘🫕🥫🍝",
        "🍜🍲🍛🍣🍱🥟🦪🍤🍙🍚",
        "🍘🍥🥠🥮🍢🍡🍧🍨🍦🥧",
        "🧁🍰🎂🍮🍭🍬🍫🍿🍩🍪",
        "🌰🥜🍯🥛🍼🫖☕️🍵🧃🥤",
        "🧋🍶🍺🍻🥂🍷🥃🍸",
        "🍹🧉🍾🧊🥄🍴🍽🥣🥡🥢",
        "😮‍💨 😤 😠 😡 🤬 🤯 😳 🥵",
        "🦱 👩‍🦰 🧑‍🦰 👨‍🦰 👱",
        "🧂a ",
        "a⚽️","a🏀","a🏈","a⚾️","a🥎","a🎾","a🏐","a🏉","a🥏","a🎱","a🪀","a🏓",
        "a🏸","a🏒","a🏑","a🥍","a🏏","a🪃","a🥅","a⛳️","a🪁","a🏹","a🎣","a🤿",
        "a🥊","a🥋","a🎽","a🛹","a🛼","a🛷","a⛸","a🥌","a🎿","a⛷","a🏂","a🪂",
        "a🏋️‍♀️","a🏋️","a🏋️‍♂️","a🤼‍♀️","a🤼","a🤼‍♂️","a🤸‍♀️",
        "a🤸","a🤸‍♂️","a⛹️‍♀️","a⛹️","a⛹️‍♂️","a🤺","a🤾‍♀️","a🤾",
        "a🤾‍♂️","a🏌️‍♀️","a🏌️","a🏌️‍♂️","a🏇","a🧘‍♀️","a🧘",
        "a🧘‍♂️","a🏄‍♀️","a🏄","a🏄‍♂️","a🏊‍♀️","a🏊","a🏊‍♂️",
        "a🤽‍♀️","a🤽","a🤽‍♂️","a🚣‍♀️",
        "a🚣","a🚣‍♂️","a🧗‍♀️","a🧗","a🧗‍♂️","a🚵‍♀️","a🚵",
        "a🚵‍♂️","a🚴‍♀️","a🚴","a🚴‍♂️","a🏆","a🥇","a🥈","a🥉",
        "a🏅","a🎖","a🏵","a🎗","a🎫","a🎟","a🎪","a🤹","a🤹‍♂️","a🤹‍♀️",
        "a🎭","a🩰","a🎨","a🎬","a🎤","a🎧",
        "a🎺","a🪗","a🎸","a🪕","a🎻",
        "a🎼","a🎹","a🥁",
        "a🪘",
        "a🎷",
        "a🎲","a♟","a🎯",
        "a🎳","a🎮","a🎰","a🧩",
        "👨‍👩‍👦",
        "🏳️‍🌈",
        "🏃🏻‍♀️  🏃🏿‍♀️ 👨‍🦰",
        "👨‍🌾 ",
        "❤️a",
        "🧡a",
        "💛a",
        "💚a",
        "💙a",
        "💜a",
        "🖤a",
        #"🩶a",
        "🤍a",
        "🤎a",
        "❤️‍🔥a",
        "❤️‍🩹a",
        "💔a",
        "❣️a",
        "💕a",
        "💞a",
        "💓a",
        "💗a",
        "💖a",
        "💘a",
        "💝a",
        "💟a",
        "☮️a",
        "✝️a",
        "☪️a",
        "🕉a",
        "☸️a",
        "✡️a",
        "🔯a",
        "🕎a",
        "☯️a",
        "☦️a",
        "🛐a",
        "⛎a",
        "♈️a",
        "♉️a",
        "♊️a",
        "♋️a",
        "♌️a",
        "♍️a",
        "♎️a",
        "♏️a",
        "♐️a",
        "♑️a",
        "♒️a",
        "♓️a",
        "🆔a",
        "⚛️a",
        "🉑a",
        "☢️a",
        "☣️a",
        "📴a",
        "📳a",
        "🈶a",
        "🈚️a",
        "🈸a",
        "🈺a",
        "🈷️a",
        "✴️a",
        "🆚a",
        "💮a",
        "🉐a",
        "㊙️a",
        "㊗️a",
        "🈴a",
        "🈵a",
        "🈹a",
        "🈲a",
        "🅰️a",
        "🅱️a",
        "🆎a",
        "🆑a",
        "🅾️a",
        "🆘a",
        "❌a",
        "⭕️a",
        "🛑a",
        "⛔️a",
        "📛a",
        "🚫a",
        "💯a",
        "💢a",
        "♨️a",
        "🚷a",
        "🚯a",
        "🚳a",
        "🚱a",
        "🔞a",
        "📵a",
        "🚭a",
        "❗️a",
        "❕a",
        "❓a",
        "❔a",
        "‼️a",
        "⁉️a",
        "🔅a",
        "🔆a",
        "〽️a",
        "✢a",
        "✣a",
        "✤a",
        "✥a",
        "✦a",
        "✧a",
        "★a",
        "☆a",
        "✯a",
        "✡︎a",
        "✩a",
        "✪a",
        "✫a",
        "✬a",
        "✭a",
        "✮a",
        "✶a",
        "✷a",
        "✵a",
        "✸a",
        "✹a",
        "→a",
        "⇒a",
        "⟹a",
        "⇨a",
        "⇾a",
        "➾a",
        "⇢a",
        "☛a",
        "☞a",
        "➔a",
        "➜a",
        "➙a",
        "➛a",
        "➝a",
        "➞a",
        "♠︎a",
        "♣︎a",
        "♥︎a",
        "♦︎a",
        "♤a",
        "♧a",
        "♡a",
        "♢a",
        "♚a",
        "♛a",
        "♜a",
        "♝a",
        "♞a",
        "♟a",
        "♔a",
        "♕a",
        "♖a",
        "♗a",
        "♘a",
        "♙a",
        "⚀a",
        "⚁a",
        "⚂a",
        "⚃a",
        "⚄a",
        "⚅a",
        "🂠a",
        "⚈a",
        "⚉a",
        "⚆a",
        "⚇a",
        "𓀀a",
        "𓀁a",
        "𓀂a",
        "𓀃a",
        "𓀄a",
        "𓀅a",
        "𓀆a",
        "𓀇a",
        "𓀈a",
        "𓀉a",
        "𓀊a",
        "𓀋a",
        "𓀌a",
        "𓀍a",
        "𓀎a",
        "𓀏a",
        "𓀐a",
        "𓀑a",
        "𓀒a",
        "𓀓a",
        "𓀔a",
        "𓀕a",
        "𓀖a",
        "𓀗a",
        "𓀘a",
        "𓀙a",
        "𓀚a",
        "𓀛a",
        "𓀜a",
        "𓀝a",
        "🏳️a",
        "🏴a",
        "🏁a",
        "🚩a",
        "🏳️‍🌈a",
        "🏳️‍⚧️a",
        "🏴‍☠️a",
        "🇦🇫a",
        "🇦🇽a",
        "🇦🇱a",
        "🇩🇿a",
        "🇦🇸a",
        "🇦🇩a",
        "🇦🇴a",
        "🇦🇮a",
        "🇦🇶a",
        "🇦🇬a",
        "🇦🇷a",
        "🇦🇲a",
        "🇦🇼a",
        "🇦🇺a",
        "🇦🇹a",
        "🇦🇿a",
        "🇧🇸a",
        "🇧🇭a",
        "🇧🇩a",
        "🇧🇧a",
        "🇧🇾a",
        "🇧🇪a",
        "🇧🇿a",
        "🇧🇯a",
        "🇧🇲a",
        "🇧🇹a",
        "🇧🇴a",
        "🇧🇦a",
        "🇧🇼a",
        "🇧🇷a",
        "🇮🇴a",
        "🇻🇬a",
        "🇧🇳a",
        "🇧🇬a",
        "🇧🇫a",
        "🇧🇮a",
        "🇰🇭a",
        "🇨🇲a",
        "🇨🇦a",
        "🇮🇨a",
        "🇨🇻a",
        "🇧🇶a",
        "🇰🇾a",
        "🇨🇫a",
        "🇹🇩a",
        "🇨🇱a",
        "🇨🇳a",
        "🇨🇽a",
        "🇨🇨a",
        "🇨🇴a",
        "🇰🇲a",
        "🇨🇬a",
        "🇨🇩a",
        "🇨🇰a",
        "🇨🇷a",
        "🇨🇮a",
        "🇭🇷a",
        "🇨🇺a",
        "🇨🇼a",
        "🇨🇾a",
        "🇨🇿a",
        "🇩🇰a",
        "🇩🇯a",
        "🇩🇲a",
        "🇩🇴a",
        "🇪🇨a",
        "🇪🇬a",
        "🇸🇻a",
        "🇬🇶a",
        "🇪🇷a",
        "🇪🇪a",
        "🇪🇹a",
        "🇪🇺a",
        "🇫🇰a",
        "🇫🇴a",
        "🇫🇯a",
        "🇫🇮a",
        "🇫🇷a",
        "🇬🇫a",
        "🇵🇫a",
        "🇹🇫a",
        "🇬🇦a",
        "🇬🇲a",
        "🇬🇪a",
        "🇩🇪a",
        "🇬🇭a",
        "🇬🇮a",
        "🇬🇷a",
        "🇬🇱a",
        "🇬🇩a",
        "🇬🇵a",
        "🇬🇺a",
        "🇬🇹a",
        "🇬🇬a",
        "🇬🇳a",
        "🇬🇼a",
        "🇬🇾a",
        "🇭🇹a",
        "🇭🇳a",
        "🇭🇰a",
        "🇭🇺a",
        "🇮🇸a",
        "🇮🇳a",
        "🇮🇩a",
        "🇮🇷a",
        "🇮🇶a",
        "🇮🇪a",
        "🇮🇲a",
        "🇮🇱a",
        "🇮🇹a",
        "🇯🇲a",
        "🇯🇵a",
        "🎌a",
        "🇯🇪a",
        "🇯🇴a",
        "🇰🇿a",
        "🇰🇪a",
        "🇰🇮a",
        "🇽🇰a",
        "🇰🇼a",
        "🇰🇬a",
        "🇱🇦a",
        "🇱🇻a",
        "🇱🇧a",
        "🇱🇸a",
        "🇱🇷a",
        "🇱🇾a",
        "🇱🇮a",
        "🇱🇹a",
        "🇱🇺a",
        "🇲🇴a",
        "🇲🇰a",
        "🇲🇬a",
        "🇲🇼a",
        "🇲🇾a",
        "🇲🇻a",
        "🇲🇱a",
        "🇲🇹a",
        "🇲🇭a",
        "🇲🇶a",
        "🇲🇷a",
        "🇲🇺a",
        "🇾🇹a",
        "🇲🇽a",
        "🇫🇲a",
        "🇲🇩a",
        "🇲🇨a",
        "🇲🇳a",
        "🇲🇪a",
        "🇲🇸a",
        "🇲🇦a",
        "🇲🇿a",
        "🇲🇲a",
        "🇳🇦a",
        "🇳🇷a",
        "🇳🇵a",
        "🇳🇱a",
        "🇳🇨a",
        "🇳🇿a",
        "🇳🇮a",
        "🇳🇪a",
        "🇳🇬a",
        "🇳🇺a",
        "🇳🇫a",
        "🇰🇵a",
        "🇲🇵a",
        "🇳🇴a",
        "🇴🇲a",
        "🇵🇰a",
        "🇵🇼a",
        "🇵🇸a",
        "🇵🇦a",
        "🇵🇬a",
        "🇵🇾a",
        "🇵🇪a",
        "🇵🇭a",
        "🇵🇳a",
        "🇵🇱a",
        "🇵🇹a",
        "🇵🇷a",
        "🇶🇦a",
        "🇷🇪a",
        "🇷🇴a",
        "🇷🇺a",
        "🇷🇼a",
        "🇼🇸a",
        "🇸🇲a",
        "🇸🇦a",
        "🇸🇳a",
        "🇷🇸a",
        "🇸🇨a",
        "🇸🇱a",
        "🇸🇬a",
        "🇸🇽a",
        "🇸🇰a",
        "🇸🇮a",
        "🇬🇸a",
        "🇸🇧a",
        "🇸🇴a",
        "🇿🇦a",
        "🇰🇷a",
        "🇸🇸a",
        "🇪🇸a",
        "🇱🇰a",
        "🇧🇱a",
        "🇸🇭a",
        "🇰🇳a",
        "🇱🇨a",
        "🇵🇲a",
        "🇻🇨a",
        "🇸🇩a",
        "🇸🇷a",
        "🇸🇿a",
        "🇸🇪a",
        "🇨🇭a",
        "🇸🇾a",
        "🇹🇼a",
        "🇹🇯a",
        "🇹🇿a",
        "🇹🇭a",
        "🇹🇱a",
        "🇹🇬a",
        "🇹🇰a",
        "🇹🇴a",
        "🇹🇹a",
        "🇹🇳a",
        "🇹🇷a",
        "🇹🇲a",
        "🇹🇨a",
        "🇹🇻a",
        "🇻🇮a",
        "🇺🇬a",
        "🇺🇦a",
        "🇦🇪a",
        "🇬🇧a",
        "🇺🇳a",
        "🇺🇸a",
        "🇺🇾a",
        "🇺🇿a",
        "🇻🇺a",
        "🇻🇦a",
        "🇻🇪a",
        "🇻🇳a",
        "🇼🇫a",
        "🇪🇭a",
        "🇾🇪a",
        "🇿🇲a",
        "🇿🇼a",
        "aa\U0000200Daa",
    ]
    invalid = [
        "😀 😃 😄 😁 😆 😅 😂 🤣 🥲 🥹 ",
        "🥶 😱 😨 😰 😥 😓 🫣 🤗 🫡 🤔 ",
        "🫢 🤭 🤫 🤥 😶 😶‍🌫️ 😐 😑",
        "😬 🫨 🫠 🙄 😯 😦 😧 😮 😲 🥱 ",
        "😴 🤤 😪 😵 😵‍💫 🫥 🤐 🥴",
        "🫰 🤟 🤘 🤙 🫵 🫱 🫲 🫸 🫷 🫳 ",
        "🫴 👈 👉 👆 🖕 👇 ☝️ 👍 👎 ✊",
        "♂️ 👸 🫅 🤴 🥷 🦸‍♀️ 🦸 🦸 ",
        " 🧚 🧚‍♂️ 🧌 👼 🤰 🫄 🫃 🤱 👩",
        "‍a",
        "a‍",
        "👤👥🫂🧳🌂☂️🧵🪡🪢🪭🧶",
        "🪯a",
        "🩵a",
        "🩷a",
        "🏴󠁧󠁢󠁥󠁮󠁧󠁿a",
        "🏴󠁧󠁢󠁳󠁣󠁴󠁿a",
        "🏴󠁧󠁢󠁷󠁬󠁳󠁿a",
        "a🪈",
        "",
        "a",
        "aaaaaaaaaaaaaaaaaaaaa",
        "aaaaaaaaaaaaaaaaaaaaaa",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "a        ",
        "        ",
        "        a",
        "    a    ",
        "a\t",
        "a卐",
        "a卍",
        "a✙",
        "a࿕",
        "a࿖",
        "aꖦ",
        "a࿗",
        "a࿘",
        "aa\U00000000aa",
        "aa\U00000001aa",
        "aa\U00000002aa",
        "aa\U00000003aa",
        "aa\U00000004aa",
        "aa\U00000000aa",
        "aa\U00000001aa",
        "aa\U00000002aa",
        "aa\U00000003aa",
        "aa\U00000004aa",
        "aa\U00000005aa",
        "aa\U00000006aa",
        "aa\U00000007aa",
        "aa\U00000008aa",
        "aa\U00000009aa",
        "aa\U0000000Aaa",
        "aa\U0000000Baa",
        "aa\U0000000Caa",
        "aa\U0000000Daa",
        "aa\U0000000Eaa",
        "aa\U0000000Faa",
        "aa\U00000010aa",
        "aa\U00000011aa",
        "aa\U00000012aa",
        "aa\U00000013aa",
        "aa\U00000014aa",
        "aa\U00000015aa",
        "aa\U00000016aa",
        "aa\U00000017aa",
        "aa\U00000018aa",
        "aa\U00000019aa",
        "aa\U0000001Aaa",
        "aa\U0000001Baa",
        "aa\U0000001Caa",
        "aa\U0000001Daa",
        "aa\U0000001Eaa",
        "aa\U0000001Faa",
        "aa\U0000007Faa",
        "aa\U00000080aa",
        "aa\U00000081aa",
        "aa\U00000082aa",
        "aa\U00000083aa",
        "aa\U00000084aa",
        "aa\U00000085aa",
        "aa\U00000086aa",
        "aa\U00000087aa",
        "aa\U00000088aa",
        "aa\U00000089aa",
        "aa\U0000008Aaa",
        "aa\U0000008Baa",
        "aa\U0000008Caa",
        "aa\U0000008Daa",
        "aa\U0000008Eaa",
        "aa\U0000008Faa",
        "aa\U00000090aa",
        "aa\U00000091aa",
        "aa\U00000092aa",
        "aa\U00000093aa",
        "aa\U00000094aa",
        "aa\U00000095aa",
        "aa\U00000096aa",
        "aa\U00000097aa",
        "aa\U00000098aa",
        "aa\U00000099aa",
        "aa\U0000009Aaa",
        "aa\U0000009Baa",
        "aa\U0000009Caa",
        "aa\U0000009Daa",
        "aa\U0000009Eaa",
        "aa\U0000009Faa",
        "aa\U000000ADaa",
        "aa\U00000600aa",
        "aa\U00000601aa",
        "aa\U00000602aa",
        "aa\U00000603aa",
        "aa\U00000604aa",
        "aa\U00000605aa",
        "aa\U0000061Caa",
        "aa\U000006DDaa",
        "aa\U0000070Faa",
        "aa\U000008E2aa",
        "aa\U0000180Eaa",
        "aa\U0000200Baa",
        "aa\U0000200Caa",
        "aa\U0000200Eaa",
        "aa\U0000200Faa",
        "aa\U0000202Aaa",
        "aa\U0000202Baa",
        "aa\U0000202Caa",
        "aa\U0000202Daa",
        "aa\U0000202Eaa",
        "aa\U00002060aa",
        "aa\U00002061aa",
        "aa\U00002062aa",
        "aa\U00002063aa",
        "aa\U00002064aa",
        "aa\U00002066aa",
        "aa\U00002067aa",
        "aa\U00002068aa",
        "aa\U00002069aa",
        "aa\U0000206Aaa",
        "aa\U0000206Baa",
        "aa\U0000206Caa",
        "aa\U0000206Daa",
        "aa\U0000206Eaa",
        "aa\U0000206Faa",
        "aa\U0000FEFFaa",
        "aa\U0000FFF9a1",
        "aa\U0000FFFAa2",
        "aa\U0000FFFBa3",
        "aa\U000110BDa4",
        "aa\U000110CDa5",
        "aa\U00013430aa",
        "aa\U00013431aa",
        "aa\U00013432aa",
        "aa\U00013433aa",
        "aa\U00013434aa",
        "aa\U00013435aa",
        "aa\U00013436aa",
        "aa\U00013437aa",
        "aa\U00013438aa",
        "aa\U0001BCA0aa",
        "aa\U0001BCA1aa",
        "aa\U0001BCA2aa",
        "aa\U0001BCA3aa",
        "aa\U0001D173aa",
        "aa\U0001D174aa",
        "aa\U0001D175aa",
        "aa\U0001D176aa",
        "aa\U0001D177aa",
        "aa\U0001D178aa",
        "aa\U0001D179aa",
        "aa\U0001D17Aaa",
        "aa\U000E0001aa",
        "aa\U000E0020aa",
        "aa\U000E0021aa",
        "aa\U000E0022aa",
        "aa\U000E0023aa",
        "aa\U000E0024aa",
        "aa\U000E0025aa",
        "aa\U000E0026aa",
        "aa\U000E0027aa",
        "aa\U000E0028aa",
        "aa\U000E0029aa",
        "aa\U000E002Aaa",
        "aa\U000E002Baa",
        "aa\U000E002Caa",
        "aa\U000E002Daa",
        "aa\U000E002Eaa",
        "aa\U000E002Faa",
        "aa\U000E0030aa",
        "aa\U000E0031aa",
        "aa\U000E0032aa",
        "aa\U000E0033aa",
        "aa\U000E0034aa",
        "aa\U000E0035aa",
        "aa\U000E0036aa",
        "aa\U000E0037aa",
        "aa\U000E0038aa",
        "aa\U000E0039aa",
        "aa\U000E003Aaa",
        "aa\U000E003Baa",
        "aa\U000E003Caa",
        "aa\U000E003Daa",
        "aa\U000E003Eaa",
        "aa\U000E003Faa",
        "aa\U000E0040aa",
        "aa\U000E0041aa",
        "aa\U000E0042aa",
        "aa\U000E0043aa",
        "aa\U000E0044aa",
        "aa\U000E0045aa",
        "aa\U000E0046aa",
        "aa\U000E0047aa",
        "aa\U000E0048aa",
        "aa\U000E0049aa",
        "aa\U000E004Aaa",
        "aa\U000E004Baa",
        "aa\U000E004Caa",
        "aa\U000E004Daa",
        "aa\U000E004Eaa",
        "aa\U000E004Faa",
        "aa\U000E0050aa",
        "aa\U000E0051aa",
        "aa\U000E0052aa",
        "aa\U000E0053aa",
        "aa\U000E0054aa",
        "aa\U000E0055aa",
        "aa\U000E0056aa",
        "aa\U000E0057aa",
        "aa\U000E0058aa",
        "aa\U000E0059aa",
        "aa\U000E005Aaa",
        "aa\U000E005Baa",
        "aa\U000E005Caa",
        "aa\U000E005Daa",
        "aa\U000E005Eaa",
        "aa\U000E005Faa",
        "aa\U000E0060aa",
        "aa\U000E0061aa",
        "aa\U000E0062aa",
        "aa\U000E0063aa",
        "aa\U000E0064aa",
        "aa\U000E0065aa",
        "aa\U000E0066aa",
        "aa\U000E0067aa",
        "aa\U000E0068aa",
        "aa\U000E0069aa",
        "aa\U000E006Aaa",
        "aa\U000E006Baa",
        "aa\U000E006Caa",
        "aa\U000E006Daa",
        "aa\U000E006Eaa",
        "aa\U000E006Faa",
        "aa\U000E0070aa",
        "aa\U000E0071aa",
        "aa\U000E0072aa",
        "aa\U000E0073aa",
        "aa\U000E0074aa",
        "aa\U000E0075aa",
        "aa\U000E0076aa",
        "aa\U000E0077aa",
        "aa\U000E0078aa",
        "aa\U000E0079aa",
        "aa\U000E007Aaa",
        "aa\U000E007Baa",
        "aa\U000E007Caa",
        "aa\U000E007Daa",
        "aa\U000E007Eaa",
        "aa\U000E007Faa",
    ]
    test_validate_fn("username", validate_username, valid, invalid)
