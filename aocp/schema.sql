CREATE TABLE IF NOT EXISTS users (
  userid TEXT PRIMARY KEY DEFAULT ( lower(hex(randomblob(6))) ),
  gitlabid TEXT UNIQUE NOT NULL,
  email TEXT NOT NULL,
  username TEXT DEFAULT NULL,
  groupid TEXT DEFAULT NULL,
  admin INTEGER DEFAULT 0,
  FOREIGN KEY (groupid) REFERENCES groups (groupid) ON DELETE SET DEFAULT
);

CREATE TABLE IF NOT EXISTS days (
  day INTEGER PRIMARY KEY AUTOINCREMENT,
  solution TEXT DEFAULT NULL,
  puzzle BLOB DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS submissions (
  userid TEXT NOT NULL,
  day INTEGER NOT NULL,
  value TEXT NOT NULL,
  status TEXT DEFAULT "PENDING",
  timestamp INTEGER DEFAULT (strftime('%s')),
  groupid TEXT DEFAULT NULL,
  PRIMARY KEY (userid, day, value),
  FOREIGN KEY (day) REFERENCES days (day),
  FOREIGN KEY (userid) REFERENCES users (userid) ON DELETE CASCADE,
  FOREIGN KEY (groupid) REFERENCES groups (groupid) ON DELETE SET DEFAULT
);

CREATE TABLE IF NOT EXISTS groups (
  groupid TEXT PRIMARY KEY NOT NULL,
  creator TEXT NOT NULL,
  color TEXT NOT NULL,
  FOREIGN KEY (creator) REFERENCES users (userid) ON DELETE CASCADE
);
