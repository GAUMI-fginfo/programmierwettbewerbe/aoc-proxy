from . import score
from . import aoc
from . import db

from enum import Flag, auto
from flask import Flask, session


class ContextReq(Flag):
    """Context Requirements.
    Different types of context information that should be available when rendering templates.

    This enum is used by build_context to determine what information should be included in the context.

    There is always automatically a 'config' field added in the context, which contains the apps config (e.g. config.json).
    """

    USERID = auto()
    """
    Include 'userid' field in the context, which is a string containing the userid of the currently logged in user or is set to None if user isn't currently logged in.
    """

    USERS = auto()
    """
    Include 'users' field in the context, which is a dict containing information about all users.
    If a user is logged in, then the 'user' field is set to the data of the current user, otherwise None.
    {
        "<userid>": {
            "username": <username | null>,
            "email": <email>,
            "groupid": <groupid | null>,
            "admin": <admin (bool)>,
            "gitlabid": <gitlabid (str)>
        }
    }
    """

    GROUPS = auto()
    """
    Include 'groups' field in the context, which is a dict containing information about all groups.
    {
        "<groupid:str>": {
            "creator": <userid:str>,
            "color": <color:str>
        }
    }
    """

    DAY = auto()
    """
    Include 'day' field in the context, which is an int containing the current AOC day (1-25),
    or 0 if the event hasn't started yet,
    or 26 if the event is already over.
    """

    LEADERBOARD = auto()
    """
    Include 'solves' field in the context, which is a dict of the solves and score for each person:
    {
        userid: {
            userid:str
            total:int
            meansubmissionseconds:int
            days:{
                day:{first:bool,score:int,solved:bool}
            }
        }
    }
    Include 'leaderboard' field in the context, which is the leaderboard as a list of the person dicts sorted (descending) by 'total' (tiebreaks are done using 'meansubmissionseconds'):
    [
        {
            userid:str
            total:int
            meansubmissionseconds:int
            days:{
                day:{first:bool,score:int,solved:bool}
            }
        },
        ...
    ]
    Include 'numsolved' field in the context, which is a dict of the number of solves for each day:
    {
        <day>: <numsolves:int>
    }
    Include 'leaderboard_groupid' field in the context, which is the groupid the leaderboard is filtered by, or none.
    """

    SUBMISSIONS = auto()
    """
    Requires: USERID

    Include 'submissions' field in the context, which is a list of dicts, sorted by timestamp (descending), containing information about all of the submissions by the current user.
    [
        {
            "day": <day:int>,
            "groupid": <groupid:str|None>,
            "status": <status:str>,
            "timestamp": <timestamp:pandas.Timestamp>,
            "userid": <userid:str>,
            "value": <value:str>,
        },
        ...
    ]
    """

    TESTING = auto()
    """
    Include various fields that are useful for testing and debugging.

    This shouldn't be used in production!
    """

    DEFAULT = USERID | USERS | GROUPS
    """
    Default requirements.
    """

    @staticmethod
    def all():
        """
        Return all context requirements except for TESTING combined.
        """
        res = ContextReq.DEFAULT
        for c in ContextReq:
            if ContextReq.TESTING not in c:
                res = res | c
        return res


def build_context(app: Flask, requirements: ContextReq = ContextReq.DEFAULT, leaderboard_groupid: str | None = None) -> dict:
    """Build the context for rendering templates.

    There are multiple possible

    """
    context = {}

    context["config"] = app.config

    if ContextReq.USERID in requirements and "userid" in session:
        context["userid"] = session["userid"]
    else:
        context["userid"] = None

    context["user"] = None
    if ContextReq.USERS in requirements:
        context["users"] = db.get_db().fetch_all_users().to_dict('index')
        if context["userid"] is not None and context["userid"] in context["users"]:
            context["user"] = context["users"][context["userid"]]
    else:
        context["users"] = {}

    if ContextReq.GROUPS in requirements:
        context["groups"] = db.get_db().fetch_all_groups().to_dict('index')
    else:
        context["groups"] = {}

    if ContextReq.DAY in requirements:
        context["day"] = aoc.get_aoc_day()[0]
    else:
        context["day"] = 0

    if ContextReq.LEADERBOARD in requirements:
        solves, leaderboard = score.get_leaderboard(leaderboard_groupid)
        context["leaderboard"] = leaderboard
        context["leaderboard_groupid"] = leaderboard_groupid
        context["solves"] = solves
        context["numsolved"] = {d: 0 for d in range(1, 26)}
        for row in leaderboard:
            for d, v in row["days"].items():
                context["numsolved"][d] += 1 if v["solved"] else 0
    else:
        context["leaderboard"] = []
        context["leaderboard_groupid"] = None
        context["solves"] = {}
        context["numsolved"] = {d: 0 for d in range(1, 26)}

    if ContextReq.SUBMISSIONS in requirements and context["userid"] is not None:
        context["submissions"] = sorted(list(db.get_db().fetch_submissions(userid=context["userid"]).reset_index().to_dict(orient="index").values()), key=lambda d: d["timestamp"], reverse=True)
    else:
        context["submissions"] = []

    if ContextReq.TESTING in requirements:
        context["users_html"] = db.get_db().fetch_all_users().to_html()

        # context["submissions_all"] = db.get_db().fetch_submissions().to_dict('split')
        context["submissions_all"] = db.get_db().fetch_submissions().to_html()
        context["submissions_me"] = db.get_db().fetch_submissions(userid=context["userid"]).to_html()
        context["submissions_someone"] = db.get_db().fetch_submissions(userid="yeet").to_html()
        context["submissions_pending"] = db.get_db().fetch_submissions(status="PENDING").to_html()
    return context
