#!/bin/bash

# Exit on error
set -e

pushd "$(dirname "$0")"
echo "Installing Requirements..."
sudo apt-get install -y bash make nginx python3 python3-venv python3-pip

echo "Setting things up..."
./setup.sh

echo "Configurating Nginx..."
sudo cp -v aoc.nginx /etc/nginx/sites-available/aoc
sudo ln -vfs /etc/nginx/sites-available/aoc /etc/nginx/sites-enabled/aoc
sudo rm -vf /etc/nginx/sites-enabled/default
sudo systemctl restart nginx

echo "Systemd..."
sudo cp -v ./gunicorn.service /etc/systemd/system/gunicorn.service
sudo systemctl daemon-reload
sudo systemctl start gunicorn
sudo systemctl enable gunicorn

echo "Changing Sudoers Permissions..."
sudo cp -v ./90-aoc-proxy-gitlab /etc/sudoers.d/90-aoc-proxy-gitlab


popd