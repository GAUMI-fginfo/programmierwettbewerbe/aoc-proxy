---
lang: de
---

# AdventOfCode Proxy




## Abhängigkeiten

```bash
sudo apt install make bash python3 python3-venv
./setup.sh
source venv/bin/activate
```
## Auf Production Installieren
Wir erwarten, dass auf dem Server c101-171.cloud.gwdg.de installiert wird mit mindestens Ubuntu 22.04.
1. `sudo apt update ; sudo apt upgrade ; sudo reboot`
1. `sudo apt install git nginx`
1. Zertifikat aufsetzen für Nginx (siehe https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal)
1. Erstelle Nutzer `gitlab`
1. Wechsel zu dem Nutzer (`su gitlab`)
1. Gehe in den Home-Ordner (`cd ~`)
1. `ssh-keygen -t ed25519` (Kein Keyphrase)
1. `cp -v .ssh/id_ed25519.pub .ssh/authorized_keys`
1. Privaten Key ausgeben: `cat .ssh/id_ed25519`
1. Den privaten Key im AOC-Proxy GitLab Repo unter Settings > CI/CD > Variables als protected Variable `SSH_PRIVATE_KEY` eintragen.
1. `git clone --recurse-submodules https://gitlab.gwdg.de/GAUMI-fginfo/programmierwettbewerbe/aoc-proxy.git`
1. `cd aoc-proxy`
1. `./install.sh` (Richtet alle ein und schmeißt alles in den Autostart.)

(Auf dem Fachgruppenserver ist für aoc.fg.informatik.uni-goettingen.de ein Reverse-Proxy zu c101-171.cloud.gwdg.de eingerichtet.)

## Datenbank resetten

Löscht die Datenbank und initialisiert sie neu.

```bash
source venv/bin/activate
make reset-db
```

## Devserver starten

```bash
source venv/bin/activate
make devserver
```

## Konfiguration

Der Flask Secret Key kann mit folgendem Befehl generiert werden:
`python -c 'import secrets; print(secrets.token_hex())'`


`instance/config.json`:
```json
{
	"SECRET_KEY": "<FLASK SECRET KEY>",
	"SESSION_COOKIE": "<AOC SESSION COOKIE>",
	"YEAR": 2023,
	"USERINTERACTIONALLOWED": <boolean whether users are allowed to do anything>
	"AUTH": {
		"SERVER_METADATA_URL": "https://gitlab.gwdg.de/.well-known/openid-configuration",
		"CLIENT_ID": "<GITLAB APPLICATION ID>",
		"CLIENT_SECRET": "<GITLAB APPLICATION SECRET>",
		"SCOPE": "openid email"
	}
}
```

## Jahreswechsel

Hier sind die Schritte dokumentiert, die jedes Jahr für den Kalender gemacht werden müssen.

### Vorbereitung (Anfang November)

1. In der Fachgruppe sollte jemand gefunden werden der sich um die **Organisation von Preisen** und so kümmert. Außerdem sollten sich um Gelder von den Fachgruppen/Fachschaften gekümmert werden.
2. Es müssen Menschen gefunden werden, die das **Plakat** designen, ausdrucken und aufhängen.
3. Im Repo müssen einige Dinge passieren:
	- Der "Programmieradventskalender 20XX beendet"-Commit muss reverted werden.
	- [datenschutz.html](aocp/templates/datenschutz.html) MUSS aktualisiert werden. (Siehe z.B. 3e96b0f7ed13a4785e76fb67036cb2129c5ffe92)
	- Ggf. in [faq.html](aocp/templates/faq.html) Wintercontest Informationen aktualisieren. (Siehe z.B. c8fd68241162ee367da3ee2b31a0d116a88f82c3)
	- Ggf. Informationen zu Preisen aktualisieren. (Siehe z.B. dfbbb3b714fc464aab7935610655f3f1907fd9d2)
	- Ggf. in [day.html](aocp/templates/fragments/day.html) die "AOC ist vorbei." Nachricht aktualisieren. Das ist abhängig davon, ob es Preise gibt. (Ja, jetzt schon. Die wird automatisch angezeigt, sobald der Kalender vorbei ist.)
4. Auf dem Server muss die `config.json` editiert werden:
	- Ein neuer `SECRET_KEY` Wert muss mit `python -c 'import secrets; print(secrets.token_hex())'` generiert werden.
	- Ein neuer `SESSION_COOKIE` Wert muss erzeugt werden, in dem man sich auf der AOC Webseite anmeldet und den Session Cookie kopiert.
	- `YEAR` muss auf das aktuelle Jahr gesetzt werden.
	- `USERINTERACTIONALLOWED` muss auf `true` gesetzt werden.
5. Server aktualisieren und **NEUSTARTEN**!

### Durchführung (01.12.)

Guck um 6 Uhr morgens das alles funktioniert, I guess.

### Gewinner kontaktieren (Ende Dezember)

1. Gewinner ermitteln, in dem man auf dem Server `source venv/bin/activate && make get-winners` ausführt.
2. Gewinner kontaktieren und sagen wo, wie, wann, und bis wann sie ihre Preise abholen können.
3. Ggf. in [day.html](aocp/templates/fragments/day.html) die "AOC ist vorbei." Nachricht aktualisieren.

### Nachbereitung (Vor dem 08.01.!!!)

1. Erstelle einen "Programmieradventskalender 20XX beendet"-Commit. (Optional: Anonymisiertes Leaderboard beibehalten.) (Siehe jeweils vorheriges Jahr.)
2. Auf dem Server muss in `config.json` `USERINTERACTIONALLOWED` auf `false` gesetzt werden.
3. Datenbank resetten, in dem man auf dem Server `source venv/bin/activate && make reset-db` ausführt.
4. Server **NEUSTARTEN**!


## Links
- https://pad.gwdg.de/J40mBSzQQ0u7EYRYg-3LnA/slide
- https://pad.gwdg.de/Le37koAnRlqE_oAx87jX2g?view
- https://flask.palletsprojects.com/en/3.0.x/tutorial/layout/
- https://flask.palletsprojects.com/en/3.0.x/tutorial/factory/
- https://flask.palletsprojects.com/en/3.0.x/tutorial/database/
- https://docs.python.org/3/library/sqlite3.html#module-sqlite3
- https://docs.python.org/3/library/sqlite3.html#module-sqlite3
- https://developer.auth0.com/resources/guides/web-app/flask/basic-authentication
- https://www.sqlite.org/lang.html

