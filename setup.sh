#!/bin/bash

# Exit on error
set -e

pushd "$(dirname "$0")"


git fetch --all
git submodule init
git submodule update
git pull --recurse-submodules

# Create and activate venv
python3 -m venv venv
source venv/bin/activate

# Install requirements
python3 -m pip install -v -r requirements.txt -U

# HTMX
if [ ! -f ./aocp/static/htmx.min.js ] ; then
    wget -O ./aocp/static/htmx.min.js "https://unpkg.com/htmx.org@1.9.9"
fi

# Initialize DB
make init-db

popd

