#!/bin/bash

set -e # Exit on error

pushd "$(dirname "$0")"
source venv/bin/activate


#gunicorn --bind 0.0.0.0:8000 -w 4 "routes:app"
gunicorn --bind 0.0.0.0:8000 -w 1 --log-file - "aocp:create_app()"
popd
